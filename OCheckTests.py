#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""

import unittest
from ocheck import ocheck
from mock import Mock, patch

CONF = {
    'srid': 4326,
    'loglevel': 'INFO',
    'frontend_url': 'http://192.168.17.10:8000',
    'frontend_token': '0de7ba1a03e036c28ef2cab19ae2555d82e175ec',
    'elastic': {
        'host': 'http://es.foo.bar',
        'port': '9020',
        'index': 'osm'
        },
    'osmrtcheck': {
        'dbname': 'osmrtcheck',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5432,
        },
    'osm2pgsql':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    }

@patch('osmelastic.osmElastic.store_node', Mock(return_value=True))
@patch('changeset.Changeset.initclasses', Mock(return_value=True))
@patch('changeset.Changeset.get', Mock(return_value={'osmid': 25733679,
                                                     'uid': 73,
                                                     'tags': {'created_by': 'iD 1.5.4',
                                                              'imagery_used': 'Bing'},
                                                     'other': {'min_lat': 45.1,
                                                               'min_lon': 11.2,
                                                               'user': 'Robert',
                                                               'closed_at': '2014-09-28T20:12:00Z',
                                                               'created_at': '2014-09-28T20:11:58Z',
                                                               'max_lat': 45.7,
                                                               'max_lon': 11.4}}))
@patch('osmstorage.osmStorage.dblog_start', Mock(return_value=None))
@patch('psycopg2.connect', Mock(return_value=None))
class OCheckTests(unittest.TestCase):

    @patch('osmstorage.osmStorage.dblog_start', Mock(return_value=None))
    @patch('psycopg2.connect', Mock(return_value=None))
    def test_init(self):
        """OSC Parser
        """
        ock = ocheck(CONF, parsefile="foo.osc")

        self.assertNotEqual(ock.rulc, None)

    @patch('rulechecker.ruleChecker.apply_tag_rules', Mock(return_value=[]))
    def test_ccnode_create(self):
        """Callback on node creation
        node is not followed
        """
        data = {'changeset': '20750033',
                'uid': '16881',
                'tags': [],
                'timestamp': '2014-02-24T12:43:09Z',
                'lon': 1.3, 'lat': 45.6,
                'version': '1',
                'geom': 'POINT(1.3 45.6)',
                'user': 'rodo', 'action': 'create',
                'type': 'node', 'id': '423'}

        ock = ocheck(CONF, parsefile="foo.osc")

        self.assertEqual(ock.ccnode(data), ([]))

    @patch('rulechecker.ruleChecker.is_followed', Mock(return_value=[]))
    def test_ccnode_delete(self):
        """Callback on node deletion
        """
        data = {'changeset': '20750033',
                'uid': '16881',
                'tags': [],
                'timestamp': '2014-02-24T12:43:09Z',
                'lon': 1.3, 'lat': 45.6,
                'version': '1',
                'geom': 'POINT(1.3 45.6)',
                'user': 'rodo', 'action': 'delete',
                'type': 'node', 'id': '423'}

        ock = ocheck(CONF, parsefile="foo.osc")

        self.assertEqual(ock.ccnode(data), ([], []))

    @patch('rulechecker.ruleChecker.is_followed', Mock(return_value=[]))
    def test_ccnode_update(self):
        """Callback on node update
        Node is not followed
        """
        data = {'changeset': '20750033',
                'uid': '16881',
                'tags': [],
                'timestamp': '2014-02-24T12:43:09Z',
                'lon': 1.3, 'lat': 45.6,
                'version': '1',
                'geom': 'POINT(1.3 45.6)',
                'user': 'rodo',
                'type': 'node', 'id': '423',
                'action': 'modify'}

        ock = ocheck(CONF, parsefile="foo.osc")

        self.assertEqual(ock.ccnode(data), ([], []))


if __name__ == '__main__':
    unittest.main()
