--
-- Changeset table partitions
--

DROP TABLE changeset0;
DROP TABLE changeset1;
DROP TABLE changeset2;
DROP TABLE changeset3;
DROP TABLE changeset4;
DROP TABLE changeset5;
DROP TABLE changeset6;
DROP TABLE changeset7;
DROP TABLE changeset8;
DROP TABLE changeset9;
DROP TABLE changeset;

CREATE TABLE changeset (
       osmid bigint,
       uid integer,
       tags json,
       other json
);
CREATE INDEX idx_changeset_osmid ON changeset (osmid);
--
--
CREATE TABLE changeset0 () INHERITS (changeset);
CREATE TABLE changeset1 () INHERITS (changeset);
CREATE TABLE changeset2 () INHERITS (changeset);
CREATE TABLE changeset3 () INHERITS (changeset);
CREATE TABLE changeset4 () INHERITS (changeset);
CREATE TABLE changeset5 () INHERITS (changeset);
CREATE TABLE changeset6 () INHERITS (changeset);
CREATE TABLE changeset7 () INHERITS (changeset);
CREATE TABLE changeset8 () INHERITS (changeset);
CREATE TABLE changeset9 () INHERITS (changeset);

--select osmid, substring(osmid::varchar from '.$') from changeset ;

CREATE OR REPLACE FUNCTION changeset_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    EXECUTE  format( 'INSERT INTO %I VALUES ( ($1).*)', concat('changeset', substring(NEW.osmid::varchar from '.$'))) USING NEW;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_changeset_trigger
    BEFORE INSERT ON changeset
    FOR EACH ROW EXECUTE PROCEDURE changeset_insert_trigger();
