--
-- Node table partitions
--

DROP TABLE node0;
DROP TABLE node1;
DROP TABLE node2;
DROP TABLE node3;
DROP TABLE node4;
DROP TABLE node5;
DROP TABLE node6;
DROP TABLE node7;
DROP TABLE node8;
DROP TABLE node9;
DROP TABLE node;

CREATE TABLE node (
       osmid bigint,
       changeset bigint,
       version smallint,
       uid integer,
       tags json,
       tms timestamp
);

SELECT AddGeometryColumn('', 'node','geom',4326,'POINT',2);

CREATE INDEX idx_node_osmid ON node (osmid);
CREATE INDEX idx_node_changeset ON node (changeset);
CREATE INDEX idx_node_uid ON node (uid);
CREATE INDEX idx_node_geom ON node (geom);

CREATE INDEX CONCURRENTLY ON node0 (osmid, version);
CREATE INDEX CONCURRENTLY ON node1 (osmid, version);
CREATE INDEX CONCURRENTLY ON node2 (osmid, version);
CREATE INDEX CONCURRENTLY ON node3 (osmid, version);
CREATE INDEX CONCURRENTLY ON node4 (osmid, version);
CREATE INDEX CONCURRENTLY ON node5 (osmid, version);
CREATE INDEX CONCURRENTLY ON node6 (osmid, version);
CREATE INDEX CONCURRENTLY ON node7 (osmid, version);
CREATE INDEX CONCURRENTLY ON node8 (osmid, version);
CREATE INDEX CONCURRENTLY ON node9 (osmid, version);

--
--
CREATE TABLE node0 () INHERITS (node);
CREATE TABLE node1 () INHERITS (node);
CREATE TABLE node2 () INHERITS (node);
CREATE TABLE node3 () INHERITS (node);
CREATE TABLE node4 () INHERITS (node);
CREATE TABLE node5 () INHERITS (node);
CREATE TABLE node6 () INHERITS (node);
CREATE TABLE node7 () INHERITS (node);
CREATE TABLE node8 () INHERITS (node);
CREATE TABLE node9 () INHERITS (node);

--select osmid, substring(osmid::varchar from '.$') from node ;

CREATE OR REPLACE FUNCTION node_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    EXECUTE  format( 'INSERT INTO %I VALUES ( ($1).*)', concat('node', substring(NEW.osmid::varchar from '.$'))) USING NEW;

    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_node_trigger
    BEFORE INSERT ON node
    FOR EACH ROW EXECUTE PROCEDURE node_insert_trigger();
