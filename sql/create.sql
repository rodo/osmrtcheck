--
--
--
DROP TABLE node;
DROP TABLE osm_user;

CREATE TABLE log (
    id serial,
    script varchar(30),
    opts varchar(255),
    start timestamp,
    stop timestamp

);


CREATE TABLE node (
       osmid bigint,
       changeset bigint,
       version smallint,
       uid integer,
       tags json,
       tms timestamp

);

SELECT AddGeometryColumn('', 'node','geom',4326,'POINT',2);

CREATE INDEX idx_node_osmid ON node (osmid);
CREATE INDEX idx_node_changeset ON node (changeset);
CREATE INDEX idx_node_uid ON node (uid);
CREATE INDEX idx_node_geom ON node (geom);
--
--
--
CREATE TABLE osm_user (
       uid integer,
       username varchar(100)
);
CREATE INDEX idx_user_osmid ON osm_user (uid);
CREATE INDEX idx_user_usernameosmid ON osm_user (username);
CREATE UNIQUE INDEX osm_user_uid_username_idx ON osm_user (uid, username);
--
--
--
CREATE TABLE changeset_user (
       changeset integer,
       uid integer
);
CREATE INDEX changeset_user_changeset_idx ON changeset_user (changeset);
CREATE INDEX changeset_user_uid_idx ON changeset_user (uid);
CREATE UNIQUE INDEX changeset_user_changeset_uid_idx ON changeset_user (changeset, uid);
