#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Parse OpenStreetMap diff files
"""
#
#
__version__ = '1.1.0'

import re
import sys
import logging
import logging.handlers
import gzip

try:
    import lxml.etree as etree
except ImportError:
    try:
        import xml.etree.ElementTree as etree
    except ImportError:
        import elementtree.ElementTree as etree


class oscParser(object):

    logger = None
    node_callback = None
    way_callback = None
    changeset_callback = None
    users = []
    nb_objects = 0
    changesets = {}

    def __init__(self, config, verbose=False, **kwargs):

        try:
            loglevel = config['loglevel']
        except KeyError:
            loglevel = 'ERROR'

        self.init_logger(loglevel)
        self.logger.debug('oscParser start, loglevel=%s' % (loglevel))
        self.nodes = {}
        self.ways = {}

    def nb_nodes(self):
        """Return number of nodes stored
        """
        return len(self.nodes)

    def init_logger(self, loglevel):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(loglevel)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def parsefile(self, filename):
        """parse a file containing xml datas

        - filename (array)
        Return:
         (int) number of element found in xml
        """
        self.logger.debug('parse file %s' % (filename[0]))
        root = self.getroot(filename[0])
        nbe = self.parse(root)
        self.logger.debug('found %s xml elements' % (nbe))
        return nbe

    def getroot(self, filename):
        # Get the first element in xml datas
        if filename.endswith('.gz'):
            content = self.readgzfile(filename)
        else:
            content = self.readfile(filename)

        try:
            tree = etree.fromstring(content)
            return tree
        except:
            msg = "Can't parse %s" % (filename)
            self.logger.critical(msg)
            sys.exit(1)

    def readgzfile(self, filename):
        """
        Read a file and return it's content

        - filename (string): the filename
        """
        # Get the first element in xml datas
        try:
            f = gzip.open(filename, 'rb')
            content = f.read()
            f.close()
            return content
        except IOError:
            self.logger.critical(errno)
            self.logger.critical("Exit here")
            sys.exit(1)
        except:
            msg = "Can't open %s" % (filename)
            self.logger.critical(msg)
            self.logger.critical("Exit here")
            sys.exit(1)

    def readfile(self, filename):
        """
        Read a file and return it's content

        - filename (string): the filename
        """
        # Get the first element in xml datas
        try:
            f = open(filename, 'r')
            content = f.read()
            f.close()
            return content
        except:
            msg = "Can't open %s" % (filename)
            self.logger.critical(msg)
            sys.exit(1)

    def parse(self, root):
        """Get the xml root
        """
        nbc = 0
        for item in root.getchildren():
            nbc = nbc + self.parse_osm(item)
        return nbc

    def parse_osm(self, node):
        # Parse an OSM object
        #
        # node : xml element modify,create or delete
        #
        nbi = 0
        for item in node.getchildren():
            nbi = nbi + 1
            key = item.attrib['changeset']
            self.changesets[key] = {'uid': item.attrib['uid'],
                                    'user': item.attrib['user']}

            self.nb_objects = self.nb_objects + 1
            elem = self.pelement(item, node.tag)
            if elem['type'] == 'node':
                self.node_callback(elem)
            if elem['type'] == 'way':
                self.way_callback(elem)
        return nbi

    def pelement(self, item, action):
        # parse an element
        #
        # action :
        #  - modify
        #  - delete
        #  - create
        #
        # Returns JSON
        tags = []
        nodes = []
        # type = node/way/relation
        # action = modify/delete/create
        attr = {'type': item.tag, 'action': action}
        i = 0
        for key in item.keys():
            try:
                if key == 'lon' or key == 'lat':
                    attr[key] = float(item.attrib[key])
                else:
                    attr[key] = item.attrib[key]
            except:
                print (key)
                print (item.keys())
                print ()
                sys.exit(1)
        i = i + 1

        # Add all the tags
        for child in item.getchildren():
            if child.tag == 'tag':
                tags.append({child.attrib['k']: child.attrib['v']})

            # a way contains nodes
            if child.tag == 'nd':
                nodes.append(child.attrib['ref'])

        # add geometry
        if item.tag == 'node':
            attr['geom'] = 'POINT({} {})'.format(attr['lon'], attr['lat'])
            self.nodes[attr['id']] = (attr['lon'], attr['lat'])

        if item.tag == 'way':
            try:
                points = ["%s %s" % (self.nodes[poo]) for poo in nodes]
                geom = 'LINESTRING (%s)' % (", ".join(points))
                attr['geom'] = geom
            except KeyError:
                attr['geom'] = ''

        attr['tags'] = tags

        return attr


    def tags_comment(self, comment):
        """Find all tag in comment

        Return an array of string
        """
        tags = re.findall('#\w+-?\w+', comment)

        return tags

    def parse_changeset(self, text):
        # Parse a changeset
        #
        #
        """<?xml version="1.0" encoding="UTF-8"?>
        <osm version="0.6" generator="OpenStreetMap server"
             copyright="..." attribution="..." license="http://open....">
          <changeset id="25733679" user="Agno-phi" uid="731498"
                     created_at="2014-09-28T20:11:58Z"
                     closed_at="2014-09-28T20:12:00Z"
                     open="false"
                     min_lat="45.7027739" min_lon="11.4768163"
                     max_lat="45.70445" max_lon="11.4785921"
                     comments_count="0">
              <tag k="created_by" v="iD 1.5.4"/>
              <tag k="imagery_used" v="Bing"/>
          </changeset>
        </osm>"""
        changeset = {}
        tags = {}
        tree = etree.fromstring(text)
        root = tree.getchildren()[0]

        osmid = int(root.attrib['id'])
        uid = int(root.attrib['uid'])
        user = root.attrib['user']

        for child in root.getchildren():
            if child.tag == 'tag':
                tags[child.attrib['k']] = child.attrib['v']

        try:
            other = {'min_lat': float(root.attrib['min_lat']),
                     'min_lon': float(root.attrib['min_lon']),
                     'max_lat': float(root.attrib['max_lat']),
                     'max_lon': float(root.attrib['max_lon'])}
        except KeyError:
            other = {}

        try:
            closed_at = root.attrib['closed_at']
        except:
            closed_at = None

        other['user'] = user
        other['created_at'] = root.attrib['created_at']
        other['closed_at'] = closed_at

        changeset = {'osmid': osmid,
                     'uid': uid,
                     'user': user,
                     'created_at': root.attrib['created_at'],
                     'closed_at': closed_at,
                     'tags': tags,
                     'other': other}

        return changeset
