#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from shapely.wkt import loads
import unittest
from mock import Mock, patch
from oscparser import oscParser

CONF = {
    'srid': 4326,
    'loglevel': 'INFO',
    'frontend_url': 'http://192.168.17.10:8000',
    'frontend_token': '0de7ba1a03e036c28ef2cab19ae2555d82e175ec',
    'osmrtcheck': {
        'dbname': 'osmrtcheck',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5432,
        },
    'osm2pgsql':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    }


XML = """<?xml version='1.0' encoding='UTF-8'?>
<osmChange version="0.6" generator="Osmosis 0.43.1">
  <modify>
    <node id="34817346" version="5" timestamp="2014-09-21T12:34:03Z"
    uid="593899" user="hpduwe" changeset="25578581"
    lat="51.8852329" lon="6.6649688"/>
    <node id="133336547" version="2" timestamp="2014-09-21T12:34:19Z"
    uid="2152895" user="spietsnaz" changeset="25578622"
    lat="15.6736223" lon="39.9546354">
      <tag k="source" v="PGS"/>
    </node>
    <node id="133336567" version="2" timestamp="2014-09-21T12:34:19Z"
    uid="2152895" user="spietsnaz" changeset="25578622" lat="1.6" lon="9.9">
      <tag k="source" v="PGS"/>
    </node>
    <node id="133336569" version="2" timestamp="2014-09-21T12:34:19Z"
    uid="2152895" user="spietsnaz" changeset="25578622"
    lat="15.6648003" lon="39.954056">
      <tag k="source" v="PGS"/>
    </node>

  </modify>
  <delete>
    <node id="3088934664" version="2" timestamp="2014-09-21T12:34:12Z"
    uid="125251" user="Bryce" changeset="25578" lat="57.498" lon="-4.120"/>
  </delete>
  <create>
    <node id="3089011665" version="1" timestamp="2014-09-21T12:35:01Z"
    uid="17422" user="meppen7" changeset="25578653" lat="45.03" lon="114.02"/>
    <node id="3212076404" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="27127029" lat="-8.95" lon="7.5"/>
    <node id="3212076405" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="27127029" lat="-8.95" lon="7.6"/>
    <node id="3212076406" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="27127029" lat="-8.9" lon="7.5"/>
    <node id="3212076407" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="27127029" lat="-8.9" lon="7.6"/>
  </create>
  <modify>
    <way id="10219799" version="15" timestamp="2014-09-21T12:34:40Z"
    uid="1850877" user="dammat" changeset="25578641">
      <nd ref="3212076406"/>
      <nd ref="3089011665"/>
      <tag k="highway" v="secondary"/>
      <tag k="ref" v="559"/>
    </way>
  </modify>
  <delete>
    <way id="39725872" version="3" timestamp="2014-09-21T12:34:22Z"
    uid="563872" user="mds878" changeset="25578577"/>
    <way id="39725874" version="4" timestamp="2014-09-21T12:34:22Z"
    uid="563872" user="mds878" changeset="25578577"/>
  </delete>
  <create>
    <way id="315110760" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="27127029">
      <nd ref="3212076404"/>
      <nd ref="3212076405"/>
      <nd ref="3212076406"/>
      <nd ref="3212076407"/>
      <nd ref="3212076404"/>
      <tag k="building" v="house"/>
    </way>
  </create>
  <modify>
    <relation id="2726919" version="5" timestamp="2014-09-21T12:34:19Z"
    uid="665748" user="sebastic" changeset="25578620">
      <member type="way" ref="203187252" role="outer"/>
      <member type="way" ref="36328824" role="outer"/>
      <member type="way" ref="36328811" role="outer"/>
      <member type="way" ref="203187233" role="outer"/>
      <member type="way" ref="203187236" role="outer"/>
      <member type="way" ref="203187257" role="outer"/>
      <tag k="admin_level" v="10"/>
      <tag k="authoritative" v="yes"/>
      <tag k="boundary" v="administrative"/>
      <tag k="name" v="Den Dolder"/>
      <tag k="ref:woonplaatscode" v="2822"/>
      <tag k="source" v="BAG"/>
      <tag k="start_date" v="2009-06-23 00:00:02"/>
      <tag k="type" v="boundary"/>
    </relation>
  </modify>
</osmChange>
"""

WAYXML = """<?xml version='1.0' encoding='UTF-8'?>
<osmChange version="0.6" generator="Osmosis 0.43.1">
  <create>
    <node id="30" version="1" timestamp="2014-09-21T12:35:01Z"
    uid="17422" user="meppen7" changeset="255" lat="45.03" lon="114.02"/>
    <node id="31" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="255" lat="40" lon="2.1"/>
    <node id="32" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="255" lat="40.1" lon="2.1"/>
    <node id="33" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="255" lat="40.1" lon="2.2"/>
    <node id="34" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="255" lat="40" lon="2.2"/>
  </create>
  <modify>
    <way id="10" version="15" timestamp="2014-09-21T12:34:40Z"
    uid="1850877" user="dammat" changeset="255">
      <nd ref="31"/>
      <nd ref="32"/>
      <nd ref="33"/>
      <nd ref="31"/>
      <tag k="highway" v="secondary"/>
      <tag k="ref" v="559"/>
    </way>
  </modify>
</osmChange>
"""

WAYXMLMISSING = """<?xml version='1.0' encoding='UTF-8'?>
<osmChange version="0.6" generator="Osmosis 0.43.1">
  <create>
    <node id="30" version="1" timestamp="2014-09-21T12:35:01Z"
    uid="17422" user="meppen7" changeset="255" lat="45.03" lon="114.02"/>
    <node id="34" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="255" lat="40" lon="2.2"/>
  </create>
  <modify>
    <way id="10" version="15" timestamp="2014-09-21T12:34:40Z"
    uid="1850877" user="dammat" changeset="255">
      <nd ref="31"/>
      <nd ref="32"/>
      <nd ref="33"/>
      <nd ref="31"/>
      <tag k="highway" v="secondary"/>
      <tag k="ref" v="559"/>
    </way>
  </modify>
</osmChange>
"""

WAYXMLNONODE = """<?xml version='1.0' encoding='UTF-8'?>
<osmChange version="0.6" generator="Osmosis 0.43.1">
  <create>
    <node id="30" version="1" timestamp="2014-09-21T12:35:01Z"
    uid="17422" user="meppen7" changeset="255" lat="45.03" lon="114.02"/>
    <node id="34" version="1" timestamp="2014-11-30T09:20:09Z"
    uid="1211085" user="каракал" changeset="255" lat="40" lon="2.2"/>
  </create>
  <modify>
    <way id="10" version="15" timestamp="2014-09-21T12:34:40Z"
    uid="1850877" user="dammat" changeset="255">
      <nd ref="31"/>
      <nd ref="32"/>
      <nd ref="33"/>
      <nd ref="31"/>
      <tag k="highway" v="secondary"/>
      <tag k="ref" v="559"/>
    </way>
  </modify>
</osmChange>
"""


class oscParserTests(unittest.TestCase):

    def foo(self, foobar):
        return None

    def test_init(self):
        """
        """
        osp = oscParser(CONF)
        self.assertEqual(osp.nb_objects, 0)

    @patch('oscparser.oscparser.oscParser.readfile', Mock(return_value=XML))
    def test_getroot(self):
        osp = oscParser(CONF)
        res = osp.getroot('foobar')
        self.assertEqual(len(res.getchildren()), 7)

    @patch('oscparser.oscparser.oscParser.readfile', Mock(return_value=XML))
    def test_parse(self):
        osp = oscParser(CONF)
        osp.node_callback = self.foo
        osp.way_callback = self.foo
        root = osp.getroot('foobar')
        osp.parse(root)
        self.assertEqual(osp.nb_objects, 15)
        self.assertEqual(len(osp.changesets), 8)
        self.assertEqual(osp.changesets['25578620'], {'uid': '665748',
                                                      'user': 'sebastic'})

    @patch('oscparser.oscparser.oscParser.readfile',
           Mock(return_value=XML))
    def test_nb_nodes(self):
        osp = oscParser(CONF)
        osp.node_callback = self.foo
        osp.way_callback = self.foo
        root = osp.getroot('foobar')
        osp.parse(root)
        self.assertEqual(osp.nb_nodes(), 10)

    @patch('oscparser.oscparser.oscParser.readfile',
           Mock(return_value=WAYXML))
    def test_pelement_node(self):
        osp = oscParser(CONF)
        osp.node_callback = self.foo
        osp.way_callback = self.foo
        root = osp.getroot('foobar')

        childs = root.getchildren()
        creates = childs[0]
        nodes = creates.getchildren()

        elmt = osp.pelement(nodes[0], 'create')

        node = {'changeset': '255',
                'uid': '17422',
                'tags': [], 'timestamp': '2014-09-21T12:35:01Z',
                'lon': 114.02,
                'lat': 45.03, 'version': '1',
                'geom': 'POINT(114.02 45.03)',
                'user': 'meppen7',
                'action': 'create',
                'type': 'node', 'id': '30'}

        self.assertEqual(elmt, node)

    @patch('oscparser.oscparser.oscParser.readfile', Mock(return_value=WAYXML))
    def test_pelement_way(self):
        osp = oscParser(CONF)
        osp.node_callback = self.foo
        osp.way_callback = self.foo
        root = osp.getroot('foobar')
        # parse whole datas to fill cache
        osp.parse(root)

        childs = root.getchildren()
        modify = childs[1]
        ways = modify.getchildren()
        elmt = osp.pelement(ways[0], 'modify')

        geom = 'LINESTRING (2.1 40.0, 2.1 40.1, 2.2 40.1, 2.1 40.0)'

        self.assertEqual(elmt['geom'], geom)

        shp = loads(elmt['geom'])

        self.assertEqual(len(shp.coords), 4)
        self.assertTrue(shp.length > 0.3)

    @patch('oscparser.oscparser.oscParser.readfile',
           Mock(return_value=WAYXMLMISSING))
    def test_pelement_way_missing(self):
        osp = oscParser(CONF)
        osp.node_callback = self.foo
        osp.way_callback = self.foo
        root = osp.getroot('foobar')
        # parse whole datas to fill cache
        osp.parse(root)

        childs = root.getchildren()
        modify = childs[1]
        ways = modify.getchildren()
        elmt = osp.pelement(ways[0], 'modify')
        shp = elmt['geom']

        self.assertEqual(shp, "")

    @patch('oscparser.oscparser.oscParser.readfile',
           Mock(return_value=WAYXMLNONODE))
    def test_pelement_way_nonode(self):
        """The way is describe with node that are no present in XML
        """
        osp = oscParser(CONF)
        osp.node_callback = self.foo
        osp.way_callback = self.foo
        root = osp.getroot('foobar')
        # parse whole datas to fill cache
        osp.parse(root)

        childs = root.getchildren()
        ways = childs[1].getchildren()

        elmt = osp.pelement(ways[0], 'modify')
        self.assertEqual(elmt['geom'], '')

    def test_tags_comment_notag(self):
        """The way is describe with node that are no present in XML
        """
        osp = oscParser(CONF)
        res = osp.tags_comment('foo bar')
        self.assertEqual(res, [])

    def test_tags_comment_onetag(self):
        """The way is describe with node that are no present in XML
        """
        osp = oscParser(CONF)
        res = osp.tags_comment('work for #projeteof-mali')
        self.assertEqual(res, ['#projeteof-mali'])

    def test_tags_comment_twotags(self):
        """Comment contains two tags
        """
        osp = oscParser(CONF)
        res1 = osp.tags_comment('work for #projeteof-mali,#hot')
        res2 = osp.tags_comment('work for #projeteof-mali, #hot')
        res3 = osp.tags_comment('#projeteof-mali and for #hot')
        res4 = osp.tags_comment('#projeteof-mali#hot')
        res5 = osp.tags_comment('#projeteof-mali;#hot')

        res = ['#projeteof-mali', '#hot']

        self.assertEqual(res1, res)
        self.assertEqual(res2, res)
        self.assertEqual(res3, res)
        self.assertEqual(res4, res)
        self.assertEqual(res5, res)

    def test_tags_comment_tagsunderscore(self):
        """Comment contains two tags
        """
        osp = oscParser(CONF)
        res1 = osp.tags_comment('work for #projeteof_mali #hot')
        self.assertEqual(res1, ['#projeteof_mali', '#hot'])

    def test_parse_changeset(self):
        xml = """<?xml version="1.0" encoding="UTF-8"?>
        <osm version="0.6" generator="OpenStreetMap server"
        copyright="OpenStreetMap and contributors"
        attribution="http://www.openstreetmap.org/copyright"
        license="http://opendatacommons.org/licenses/odbl/1-0/">
          <changeset id="25733679" user="Agno-phi" uid="73"
          created_at="2014-09-28T20:11:58Z"
          closed_at="2014-09-28T20:12:00Z" open="false"
          min_lat="45.1" min_lon="11.2" max_lat="45.7" max_lon="11.4"
          comments_count="0">
              <tag k="created_by" v="iD 1.5.4"/>
              <tag k="imagery_used" v="Bing"/>
          </changeset>
        </osm>"""

        osp = oscParser(CONF)
        res1 = osp.parse_changeset(xml)
        self.assertEqual(res1, {'closed_at': '2014-09-28T20:12:00Z',
                                'created_at': '2014-09-28T20:11:58Z',
                                'osmid': 25733679,
                                'uid': 73,
                                'user': 'Agno-phi',
                                'tags': {'created_by': 'iD 1.5.4',
                                         'imagery_used': 'Bing'},
                                'other': {'closed_at': '2014-09-28T20:12:00Z',
                                          'created_at': '2014-09-28T20:11:58Z',
                                          'min_lat': 45.1,
                                          'min_lon': 11.2,
                                          'max_lat': 45.7,
                                          'max_lon': 11.4,
                                          'user': 'Agno-phi'}})
