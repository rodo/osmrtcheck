#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""

import unittest
from mock import Mock, patch
from rulechecker import Alert
from rulechecker import Rule
from rulechecker import ruleChecker

RULES = [{u'name': u'Only node',
          u'tag_regex': u'amenity=bicycle_parking',
          u'way_applied': False,
          u'node_applied': True,
          u'domains': [2],
          u'id': 4},
         {u'name': u'Nodes and ways',
          u'tag_regex': u'advertising=billboard',
          u'way_applied': True,
          u'node_applied': True,
          u'domains': [1],
          u'id': 5},
         ]

ALERTS = [{"id": 3, "name": "European earthquake", "domain": 1, "geozone": 1},
          {"id": 4, "name": "German Worm disease", "domain": 2, "geozone": 1}]

# As returned by util.jsonzone
#
ZONE = {"crs": {"type": "link",
                "properties": {"href": "http://spatial.org/ref/epsg/4326/",
                               "type": "proj4"}},
        "type": "FeatureCollection",
        "features": [{"geometry": {"type": "MultiPolygon",
                                   "coordinates": [[[
                                       [-72.081298, 18.52128],
                                       [-72.081298, 18.54211],
                                       [-72.053832, 18.54211],
                                       [-72.053832, 18.52128],
                                       [-72.081298, 18.52128]
                                       ]]]},
                      "type": "Feature",
                      "properties": {"model": "alerts.geozone"},
                      "id": 1}]}


class RulecheckerTests(unittest.TestCase):

    def test_init_alert(self):
        """Test class Alert
        """
        alert = Alert({'id': 2, 'geozone': 1})

        self.assertEqual(alert.id, 2)
        self.assertEqual(alert.cached, False)
        self.assertIsNone(alert.geozone_json)

    def test_initload_alert(self):
        """Test class Alert
        """
        alert = Alert({'id': 2, 'geozone': 1})

        alert.geozone_fromjson(ZONE)

        self.assertEqual(alert.id, 2)
        self.assertEqual(alert.geozone_id, 1)
        self.assertEqual(alert.geozone_json, ZONE)

    def test_init_rule(self):
        """Test class Rule
        """
        rule = Rule({'id': 2,
                     'domains': 31})

        self.assertEqual(rule.id, 2)
        self.assertEqual(rule.domains, 31)

    def test_init(self):
        """
        """

        ruc = ruleChecker("http://url.foo", "foo_token")
        self.assertEqual(ruc.action, None)
        self.assertEqual(ruc.element, None)
        self.assertEqual(len(ruc.cache_alerts), 0)
        self.assertEqual(len(ruc.cache_geozones), 0)
        self.assertEqual(len(ruc.rules), 0)

    def test_init_with_action(self):
        """Init rulechecker with an action
        """
        ruc = ruleChecker("http://url.foo", "foo_token",
                          action="delete",
                          element="node")
        self.assertEqual(ruc.action, "delete")
        self.assertEqual(ruc.element, "node")

    @patch('util.jsonrequ', Mock(return_value=(200, RULES)))
    def test_fetch_rules(self):
        """
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        res = ruc.fetch_rules("create", "node")

        self.assertEqual(res, (200, RULES))

    @patch('util.jsonrules', Mock(return_value=(200, RULES)))
    def test_get_rules(self):
        """Retreive rules cached or not
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        ruc.get_rules("create", "node")

        self.assertEqual(len(ruc.rules), 1)

    @patch('util.jsonrules', Mock(return_value=(200, RULES)))
    def test_get_rulestwice(self):
        """
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        ruc.get_rules("create", "node")
        ruc.get_rules("create", "node")

        self.assertEqual(len(ruc.rules), 1)

    @patch('util.jsonalerts', Mock(return_value=(200, ALERTS)))
    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_fetch_alerts(self):
        """Fetch alerts from domain ids 1,2,3
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        res = ruc.fetch_alerts([1, 2, 3])

        first_alert = res[0]
        zone = first_alert.geozone_json

        self.assertEqual(len(res), 6)
        self.assertEqual(first_alert.cached, False)
        self.assertEqual(first_alert.id, 3)
        self.assertEqual(first_alert.geozone_id, 1)
        self.assertEqual(len(zone['features']), 1)

    @patch('util.jsonalerts', Mock(return_value=(200, ALERTS)))
    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_get_alerts(self):
        """Retreive alerts from a domain
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        res = ruc.get_alerts([1])
        first_alert = res[0]

        res = ruc.get_alerts([1])
        first_alert = res[0]

        self.assertEqual(len(res), 2)
        self.assertEqual(first_alert.cached, True)

    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_get_geozone(self):
        """
        """
        ruc = ruleChecker("http://url.foo", "foo_token")

        self.assertEqual(len(ruc.cache_geozones), 0)

    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_get_geozone_set(self):
        """
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        geoz = ruc.get_geozone(100)
        self.assertEqual(len(ruc.cache_geozones), 1)
        self.assertEqual(geoz.keys(), ['crs', 'type', 'features'])

    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_get_geozonetwice(self):
        """
        """
        ruc = ruleChecker("http://url.foo", "foo_token")
        ruc.get_geozone(100)
        ruc.get_geozone(200)
        ruc.get_geozone(200)
        self.assertEqual(len(ruc.cache_geozones), 2)

    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_isin_zone(self):
        ruc = ruleChecker("http://url.foo", "foo_token")

        rule = Rule({'id': 34,
                     'domains': [4, 3]})

        zone2 = {"crs": {"type": "link",
                         "properties": {"href": "http://spatial.org/4326/",
                                        "type": "proj4"}},
                 "type": "FeatureCollection",
                 "features": [{"geometry": {"type": "MultiPolygon",
                                            "coordinates": [[[
                                                [-16.69, 38.438037],
                                                [-7.382, 51.835248],
                                                [12.128, 53.120657],
                                                [17.226, 49.721984],
                                                [25.839, 38.439037],
                                                [-16.65, 38.439037]]]]},
                               "type": "Feature",
                               "properties": {"model": "alerts.geozone"},
                               "id": 1}]}

        zone3 = {"crs": {"type": "link",
                         "properties": {"href": "http://spatialrsg/4326/",
                                        "type": "proj4"}},
                 "type": "FeatureCollection",
                 "features": [{"geometry": {"type": "MultiPolygon",
                                            "coordinates": [[[
                                                [-16.6, 38.4],
                                                [-7.3, 51.8],
                                                [12.12, 53.12],
                                                [-16.69, 38.43]]]]},
                               "type": "Feature",
                               "properties": {"model": "alerts.geozone"},
                               "id": 1}]}

        alert2 = Alert({'id': 2, 'geozone': 1})
        alert2.geozone_fromjson(zone2)

        alert3 = Alert({'id': 3, 'geozone': 1})
        alert3.geozone_fromjson(zone3)

        rule.alerts = [alert2, alert3]

        element = {'changeset': '21574342', 'uid': '1507370',
                   'tags': [],
                   'timestamp': '2014-04-08T16:37:25Z',
                   'lon': 18.3426997, 'lat': 46.4658688,
                   'version': '1', 'geom': 'POINT(18.3426997 46.4658688)',
                   'user': 'Cpt Balu', 'action': 'create', 'type': 'node',
                   'id': '2777484194'}

        res = ruc.isin_zone(rule, element)

        self.assertEqual(rule.nb_alerts(), 2)
        self.assertEqual(res, [2])

    @patch('util.jsonrules', Mock(return_value=(200, RULES)))
    @patch('util.jsonalerts', Mock(return_value=(200, ALERTS)))
    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_is_followed(self):
        """Is the object is not in a followed geozone
        """
        data = {'changeset': '20750033',
                'uid': '16881',
                'tags': [],
                'timestamp': '2014-02-24T12:43:09Z',
                'lon': 1.3, 'lat': 45.6,
                'version': '1',
                'geom': 'POINT(1.3 45.6)',
                'user': 'rodo',
                'type': 'node', 'id': '423',
                'action': 'delete'}

        ruc = ruleChecker("http://url.foo", "foo_token")

        res = ruc.is_followed(data, "delete", "node")

        self.assertEqual(res, [])

    @patch('util.jsonrules', Mock(return_value=(200, RULES)))
    @patch('util.jsonalerts', Mock(return_value=(200, ALERTS)))
    @patch('util.jsonzone', Mock(return_value=(200, ZONE)))
    def test_is_followed_ok(self):
        """Is the object is not in a followed geozone
        """
        data = {'changeset': '20750033',
                'uid': '16881',
                'tags': [],
                'timestamp': '2014-02-24T12:43:09Z',
                'lon': -72.06, 'lat': 18.53,
                'version': '1',
                'geom': 'POINT(-72.06 18.53)',
                'user': 'rodo',
                'type': 'node', 'id': '423',
                'action': 'delete'}

        ruc = ruleChecker("http://url.foo", "foo_token")

        res = ruc.is_followed(data, "delete", "node")

        self.assertEqual(res, [3, 4])
