# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import logging
import json
import logging.handlers
from oscparser.oscparser import oscParser
from osmstorage import osmStorage
from osmdl import osmDownloader
from osmelastic import osmElastic

__version__ = "1.0.0"


class Changeset(object):

    logger = None

    def __init__(self, config, **kwargs):
        self.config = config

        #  cache initialisation
        self.cache_size = 1000
        self.cache_store = {}

        try:
            loglevel = config['loglevel']
        except KeyError:
            loglevel = 'ERROR'

        self.init_logger(loglevel)
        self.initclasses(loglevel)
        self.logger.debug("init")

    def initclasses(self, loglevel):
        #  instantiate a db
        self.dbstore = osmStorage(self.config, loglevel=loglevel)

        self.elastic = osmElastic(self.config, loglevel=loglevel)

        #  instantiate a downloader
        self.dl = osmDownloader(self.config, loglevel=loglevel)

        #  instantiate a parser
        self.parser = oscParser(self.config, loglevel=loglevel)

    def stop(self):
        self.dbstore.stop()

    def init_logger(self, loglevel):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger("changeset")

        self.logger.setLevel(loglevel)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.logger.info("START")

    def get(self, osmid):
        """Get a changeset

        Return : dict
        """
        if str(osmid) in self.cache_store:
            #  get from local cache
            chg = self.cache_store[str(osmid)]
        else:
            #  get from db
            chg = self.dbstore.get_changeset(osmid)

        if chg is None:
            #  get from API
            chg = self.download(osmid)

            if chg is not None:
                self.store(chg)
                self.cache(chg)
        return chg

    def cache(self, chg):
        """

        :param: chg dict
        """
        if len(self.cache_store) > self.cache_size:
            key = self.cache_store.keys()[0]
            del self.cache_store[key]

        self.cache_store[str(chg['osmid'])] = chg

    def download(self, osmid):
        """Download a changeset from API
        """
        content = self.dl.fetch_changeset(osmid)
        if content:
            chg = self.parser.parse_changeset(content)

        return chg

    def parse(self, fpath):
        """Parse a changeset
        """
        f = open(fpath, 'r')
        content = f.read()
        f.close()

        chg = self.parser.parse_changeset(content)

        return chg

    def store(self, chg):
        """Store a changeset in database and elastic

        :param: osmid biginteger
        :param: uid biginteger
        :param: tags dict
        :param: other dict
        """
        res = self.dbstore.store_changeset(chg['osmid'],
                                           chg['uid'],
                                           json.dumps(chg['tags']),
                                           json.dumps(chg['other']))

        self.elastic.store_changeset(chg)

if __name__ == '__main__':
    from config import conf
    chg = Changeset(conf)
    chg.get(sys.argv[1])
