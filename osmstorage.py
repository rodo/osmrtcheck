# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import ppygis
import json
import logging
import logging.handlers
from os import path, mkdir
from oshparser.oshparser import oshParser
from osmdl import osmDownloader


class osmStorage(object):

    osmdl = None
    loglevel = logging.CRITICAL

    def __init__(self, conf, **kwargs):
        self.init_logger(**kwargs)
        self.conf = conf
        self.logger.debug("dsn {}".format(self.get_dsn()))
        self.osmdl = osmDownloader(loglevel=self.loglevel)
        self.conn = None
        self.conn_stat = None
        self.node_store = 0
        self.osh = oshParser(loglevel=self.loglevel)

    def stop(self):
        self.osmdl.stop()
        self.logger.info("%s nodes store in db" % (self.node_store))
        self.logger.info("stop normally")

    def dbconnect(self):
        """Connect to database
        """
        self.conn = psycopg2.connect(self.get_dsn())

    def init_logger(self, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger("osmStorage")

        if 'loglevel' in kwargs:
            self.loglevel = kwargs['loglevel']

        self.logger.setLevel(self.loglevel)
        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def get_dsn(self, base='osmrtcheck'):
        dsn = "host='%s' dbname='%s' user='%s' password='%s' port=%s"

        return dsn % (self.conf[base]['dbhost'],
                      self.conf[base]['dbname'],
                      self.conf[base]['dbuser'],
                      self.conf[base]['dbpass'],
                      self.conf[base]['dbport'])

    def seqpath(self, sequenceNumber):
        """Build the path from sequence number
        """
        num = "%012d" % (int(sequenceNumber))
        fpath = "{}/{}/{}".format(num[0:3], num[3:6], num[6:9])
        return fpath

    def createdir(self, fpath):
        """Create directories where state and diff files will be stored

        fpath (string) : file path
        """
        root_path = ''
        for xpat in fpath.split('/'):
            root_path = path.join(root_path, xpat)
            if not path.isdir(root_path):
                self.logger.debug("create dir {}".format(root_path))
                mkdir(root_path)

        return path.isdir(fpath)

    def fetch_previous_version(self, objtype, osmid, version):
        """Fetch from online server
        http://www.openstreetmap.org/api/0.6/node/276729647

        objtype : string (node, way, relation)
        osmid : integer
        """
        if self.conn is None:
            self.dbconnect()
        rpath = None
        xmlpath = path.join('data', objtype, self.seqpath(osmid))
        osm = []

        if (self.createdir(xmlpath)):
            fpath = path.join(xmlpath, "%s.osm" % osmid)
            rpath = self.osmdl.fetch_node(osmid, fpath)
            if rpath:
                # Parse history file
                self.logger.debug("Parse history file %s" % rpath)
                self.osh.parsefile(rpath)
                for data in self.osh.datas:
                    # deleted object are not stored
                    if data['visible']:
                        self.store_node(data)
                        osm.append(data)
        return osm

    def get_previous_version(self, objtype, osmid, version):
        """Get the previous version
        """
        if self.conn is None:
            self.dbconnect()
        tags = []
        geom = ""
        changeset = 0
        result = "notfound"

        if version >= 1:
            cur = self.conn.cursor()
            query = " ".join(["SELECT tags, ST_AsText(geom), changeset",
                              "FROM node",
                              "WHERE osmid=%s and version = %s"])
            cur.execute(query, (osmid, version - 1))

            datas = cur.fetchone()
            if datas is not None:
                try:
                    result = 'found'
                    tags = datas[0]
                    geom = datas[1]
                    changeset = int(datas[2])
                except:
                    pass
            else:
                if version > 1:
                    self.fetch_previous_version(objtype, osmid, version)

            cur.close()

        return {'result': result,
                'changeset': changeset,
                'tags': tags,
                'geom': geom}

    def store_node(self, node):
        """Store a node in DB
        """
        if self.conn is None:
            self.dbconnect()
        cur = self.conn.cursor()
        # Point: Lon, Lat
        point = ppygis.Point(node['lon'], node['lat'])
        point.srid = self.conf['srid']
        self.logger.debug("Store node {} version {}".format(node['id'],
                                                            node['version']))
        query = "".join("INSERT INTO node "
                        "(osmid, changeset, version, uid, tms, tags, geom) "
                        "VALUES (%s, %s, %s, %s, %s, %s, %s)")

        node = self.check_node(node)
        self.node_store = self.node_store + 1
        try:
            data = (node['id'],
                    node['changeset'],
                    node['version'],
                    node['uid'],
                    node['timestamp'],
                    json.dumps(node['tags']),
                    point
                    )
            cur.execute(query, data)
            self.conn.commit()
        except:
            self.logger.critical("error when store %s" % (node))
            sys.exit(1)

        return node

    def check_node(self, node):
        """Check datas in nodes
        Sometimes nodes have no uid
        <node id="15824841" changeset="113136" timestamp="2006-09-17T10:51:29Z"
              version="1" visible="true"
              lat="51.8937364"
              lon="-2.0646823">
          <tag k="created_by" v="JOSM"/>
        </node>
        <node id="15824841" changeset="481956" timestamp="2007-11-12T07:34:55Z"
              version="2" visible="true"
              user="Richard" uid="165"
              lat="51.8937567"
              lon="-2.0644499">
          <tag k="created_by" v="JOSM"/>
        </node>
        """
        node['uid'] = node.get('uid', 0)
        node['user'] = node.get('user', 'anonymous')
        return node

    def store_changesets(self, changesets):
        """Store changesets in DB

        - changesets (dict): key, changeset id, value (object) {'uid', 'user'}
        """

        if self.conn is None:
            self.dbconnect()
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = self.conn.cursor()
        users = {}

        self.logger.info("Store changesets")
        query = " ".join(["INSERT INTO changeset_user",
                          "(changeset, uid) "
                          "VALUES (%s, %s)"])
        for chg, user in changesets.iteritems():
            users[user['uid']] = user['user']
            try:
                cur.execute(query, (chg, user['uid']))
            except psycopg2.IntegrityError:
                pass

        query = " ".join(["INSERT INTO osm_user",
                          "(uid, username) "
                          "VALUES (%s, %s)"])
        for uid, user in users.iteritems():
            try:
                cur.execute(query, (uid, user))
            except psycopg2.IntegrityError:
                pass

    def users_unknown(self):
        """
        """
        if self.conn is None:
            self.dbconnect()
        userids = []
        cur = self.conn.cursor()
        self.logger.info("Read unknown users")
        query = " ".join(["SELECT DISTINCT(node.uid) FROM node "
                          "LEFT JOIN osm_user ON osm_user.uid = node.uid"
                          "WHERE username IS NULL"])
        cur.execute(query)
        for user in cur.fetchall():
            userids.append(user[0])

        return userids

    def store_stats(self, stats):
        """Store changesets in DB

            stats.append((1,'deleted',date,uid,alertid))
        """
        if not self.conn_stat:
            self.conn_stat = psycopg2.connect(self.get_dsn('osmstat'))
        self.conn_stat.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = self.conn_stat.cursor()

        self.logger.info("Store stats")
        query = " ".join(["INSERT INTO stats_alertuserstats",
                          "(item, date_stat, userid, alert_id, created,",
                          "modified, deleted, month, year) ",
                          "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"])

        qdel = " ".join(["DELETE FROM stats_alertuserstats WHERE",
                         "item=%s AND date_stat=%s",
                         "AND userid=%s AND alert_id=%s"])
        riq = 0
        for stat in stats:
            cur.execute(qdel, (stat[0:4]))
            cur.execute(query, (stat))
            riq += 1
        return riq

    def dblog_start(self, script, opts):
        """Store a log start

        opts (dict) : options
        """
        if self.conn is None:
            self.dbconnect()

        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = self.conn.cursor()

        query = " ".join(["INSERT INTO log(script, start, options)",
                          "VALUES (%s, now(), %s) RETURNING id"])

        cur.execute(query, (script, json.dumps(opts)))
        row = cur.fetchone()
        return row[0]

    def dblog_stop(self, logid):
        """Store a log stop in database
        """
        if self.conn is None:
            self.dbconnect()
        
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = self.conn.cursor()

        query = "UPDATE log SET stop=now() WHERE id=%s"

        cur.execute(query, (logid,))

    def get_changeset(self, osmid):
        """Get a changeset from database
        """
        result = "notfound"

        table_id = str(osmid)[-1:]

        if self.conn is None:
            self.dbconnect()

        try:
            cur = self.conn.cursor()
        except:
            sys.stderr.write("ERROR can't get cursor\n")
            sys.stderr.write(self.get_dsn())
            sys.exit(1)
        query = " ".join(["SELECT osmid, uid, tags, other",
                          "FROM changeset{}",
                          "WHERE osmid=%s"])
        cur.execute(query.format(table_id), (osmid, ))
        datas = cur.fetchone()
        cur.close()
        if datas is not None:
            result = {'result': 'found',
                      'osmid': datas[0],
                      'uid': datas[1],
                      'tags': datas[2],
                      'other': datas[3]}
        else:
            result = None

        return result

    def store_changeset(self, osmid, uid, tags=None, other=None):
        """Store a changeset in database
        """
        table_id = str(osmid)[-1:]

        cur = self.conn.cursor()
        query = " ".join(["INSERT INTO changeset{} (osmid, uid, tags, other)",
                          "VALUES (%s, %s, %s, %s)"])
        result = cur.execute(query.format(table_id), (osmid, uid, tags, other))
        self.conn.commit()
        return result
