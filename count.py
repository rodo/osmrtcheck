#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Count elements in openstreetmap osc file transformed with osmconvert
"""
import psycopg2
import time
from datetime import datetime
from lxml import etree
import json
import sys
import logging
import logging.handlers
from optparse import OptionParser
from osmreplicat import oscname, statepath

__version__ = "1.0.1"

def main(fpath, fpathseq, opts, dsn):

    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)

    handler = logging.handlers.SysLogHandler(address='/dev/log',facility='user')

    my_logger.addHandler(handler)

    try:
        conn = psycopg2.connect(dsn)
    except:
        print "Unable to connect to database {}".format(dsn)
        sys.exit(1)

    # Read the sequence number
    f = open(fpathseq, 'r')
    data = f.read()
    (sequence, dt) = data.split(',')
    f.close()

    if opts.verbose:
        print "sequence {} date {}".format(sequence, dt)
        print "open {}".format(fpathseq)
        print "open {}".format(fpath)

    tree = etree.parse(fpath)

    dt = datetime.strptime(dt, '%Y-%m-%dT%H\:%M\:%SZ')
    dt = dt.strftime('%s')

    # Insert datas in tables
    for table in (u'node', u'way', u'relation'):
        users = {}
        tags = {}
        for action in (u'modify', u'create', u'delete'):
            for obj in tree.xpath("/osmChange/{}/{}".format(action, table)):
                user = obj.get('uid')
                if user in users:
                    users[user] = users[user] + 1
                else:
                    users[user] = 1
                for tag in obj.findall('tag'):
                    key = tag.get('k')
                    tags[key] = tags.get(key, 0) + 1

        xmodify = len(tree.xpath("/osmChange/modify/{}".format(table)))
        xcreate = len(tree.xpath("/osmChange/create/{}".format(table)))
        xdelete = len(tree.xpath("/osmChange/delete/{}".format(table)))

        if opts.verbose:
            print "Found in {:<8} {:>5} {:>5} {:>5}".format(table, xcreate, xmodify, xdelete)

        cur = conn.cursor()

        sql = "DELETE FROM {}s WHERE sequence=%s".format(table)
        cur.execute(sql, (sequence,))

        sql = "INSERT INTO {}s (date, sequence, modified, created, deleted, users, tags ) VALUES (%s,%s,%s,%s,%s,%s,%s)"
        values = (dt, sequence, xmodify, xcreate, xdelete, len(users), json.dumps(tags) )

        try:
            cur.execute(sql.format(table), values)
        except:
            my_logger.critical(sql % values)
            pass

        sql = "DELETE FROM tags WHERE sequence=%s".format(table)
        cur.execute(sql, (sequence,))

        for key in tags:
            sql = "INSERT INTO tags (objects, date, sequence, tag, number) VALUES (%s,%s,%s,%s,%s)"
            values = (table, dt, sequence, key, tags[key])
            try:
                cur.execute(sql, values)
            except Exception as inst:
                my_logger.critical(type(inst))
                my_logger.critical(sql % values)
                pass


    # Save (commit) the changes
    conn.commit()

    # not when working on old stats
    if opts.sequence == 0:
        sql = 'SELECT date, modified, created, deleted FROM {} ORDER BY date DESC limit 1500'

        for table in ('nodes', 'ways', 'relations'):
            modified = []
            created = []
            deleted = []

            res = cur.execute(sql.format(table))

            for row in cur.fetchall():
                modified.insert(0, {"x": row[0], "y": row[1]})
                created.insert(0, {"x": row[0], "y": row[2]})
                deleted.insert(0, {"x": row[0], "y": row[3]})


            datas = [{"name": "modified", "data": modified},
                     {"name": "created", "data": created},
                     {"name": "deleted", "data": deleted}
                     ]

            f = open('data_{}.json'.format(table),'w')
            f.write(json.dumps(datas))
            f.close()


    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()

def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-s SEQUENCE] [-v]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)

    parser.add_option("-s", "--sequence", dest="sequence",
                      type=int,
                      help="graphite hostname",
                      default=0)
    parser.add_option("-d", "--dbname", dest="dbname",
                      help="database name",
                      default='oscstats')
    parser.add_option("-p", "--port", dest="port",
                      help="database port",
                      default='5432')
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="be verbose",
                      default=False)

    return parser.parse_args()[0]


if __name__ == "__main__":

    options = arg_parse()

    dsn = "dbname={} port={}".format(options.dbname,
                                     options.port)

    if options.sequence == 0:
        main("lastdata.osc", "sequence.txt", options, dsn)
    else:
        oscpath = oscname(options.sequence)
        seq = statepath(options.sequence)
        main(oscpath, seq, options)
