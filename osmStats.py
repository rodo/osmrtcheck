# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
#
#
#
#
import logging
import logging.handlers


class osmStats(object):

    osmdl = None
    loglevel = logging.CRITICAL

    def __init__(self, **kwargs):
        self.init_logger(**kwargs)
        self.logger.info("start osmstats")
        self.nodes_keys = {}
        self.nodes_delete = {}
        self.nodes_modify = {}
        self.nodes_create = {}
        self.ways_keys = {}
        self.ways_delete = {}
        self.ways_modify = {}
        self.ways_create = {}

    def init_logger(self, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger("osmStats")

        if 'loglevel' in kwargs:
            self.loglevel = kwargs['loglevel']

        self.logger.setLevel(self.loglevel)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def mkey(self, timestamp, user, alert_id):
        """Create a unique key
        """
        dt = timestamp[0:10]
        key = '%s-%s-%s' % (dt, str(alert_id), str(user))
        return key

    def node_delete(self, timestamp, user, alert_id):
        """A node is deleted

        Return : (tuple)
        """
        key = self.mkey(timestamp, user, alert_id)
        self.nodes_keys[key] = key
        value = self.nodes_delete.get(key, 0)
        self.nodes_delete[key] = value + 1
        return (len(self.nodes_delete), key)

    def node_modify(self, timestamp, user, alert_id):
        """A node is modifyd

        Return : (tuple)
        """
        key = self.mkey(timestamp, user, alert_id)
        self.nodes_keys[key] = key
        value = self.nodes_modify.get(key, 0)
        self.nodes_modify[key] = value + 1
        return (len(self.nodes_modify), key)

    def node_create(self, timestamp, user, alert_id):
        """A node is created

        Return : (tuple)
        """
        key = self.mkey(timestamp, user, alert_id)
        self.nodes_keys[key] = key
        value = self.nodes_create.get(key, 0)
        self.nodes_create[key] = value + 1

        return (len(self.nodes_create), key)

    def way_delete(self, timestamp, user, alert_id):
        """A way is deleted

        Return : (tuple)
        """
        key = self.mkey(timestamp, user, alert_id)
        self.ways_keys[key] = key
        value = self.ways_delete.get(key, 0)
        self.ways_delete[key] = value + 1
        return (len(self.ways_delete), key)

    def way_modify(self, timestamp, user, alert_id):
        """A way is modified

        Return : (tuple)
        """
        key = self.mkey(timestamp, user, alert_id)
        self.ways_keys[key] = key
        value = self.ways_modify.get(key, 0)
        self.ways_modify[key] = value + 1
        return (len(self.ways_modify), key)

    def way_create(self, timestamp, user, alert_id):
        """A way is created

        Return : (tuple)
        """
        key = self.mkey(timestamp, user, alert_id)
        self.ways_keys[key] = key
        value = self.ways_create.get(key, 0)
        self.ways_create[key] = value + 1

        return (len(self.ways_create), key)

    def store(self):
        """Merge all stats in one line and return new format

        """
        stats = []
        for key in self.nodes_keys.keys():
            datas = str(key).split('-')
            date = "-".join(datas[0:3])
            alertid = datas[3]
            uid = datas[4]
            # 1 is for node
            stat = (1,
                    date,
                    int(uid),
                    int(alertid),
                    self.nodes_create.get(key, 0),
                    self.nodes_modify.get(key, 0),
                    self.nodes_delete.get(key, 0),
                    int(date[5:7]),
                    int(date[0:4]))
            stats.append(stat)

        for key in self.ways_keys.keys():
            datas = str(key).split('-')
            date = "-".join(datas[0:3])
            alertid = datas[3]
            uid = datas[4]
            # 1 is for way
            stat = (2,
                    date,
                    int(uid),
                    int(alertid),
                    self.ways_create.get(key, 0),
                    self.ways_modify.get(key, 0),
                    self.ways_delete.get(key, 0),
                    int(date[5:7]),
                    int(date[0:4]))
            stats.append(stat)

        self.logger.info("will store %s stats" % (len(stats)))
        return stats
