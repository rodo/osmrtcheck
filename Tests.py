#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2013,2014 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from test_osmReplicat import ReplicatTests  # NoQa
from test_osmStats import osmStatsTests  # NoQa
from OCheckTests import OCheckTests  # NoQa
from test_ruleChecker import RulecheckerTests  # NoQa
from test_osmstorage import osmStorageTests  # NoQa
from test_osmdl import osmDownloaderTests  # NoQa
from test_changeset import changesetTests  # NoQa
