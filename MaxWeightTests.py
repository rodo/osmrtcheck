#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from checker import MaxWeight


class MaxWeightTests(unittest.TestCase):

    def test_ok_correct(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("2.4")
        self.assertTrue(valid)

    def test_ok_correct1(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("200")
        self.assertTrue(valid)

    def test_ok_correct2(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("0.4")
        self.assertTrue(valid)

    def test_ok_correct_unit(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("2.4 t")
        self.assertTrue(valid)

    def test_ok_exclusion(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("FIXME")
        self.assertTrue(valid)


    def test_ko_negative_value(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("-2.4")
        self.assertFalse(valid)

    def test_ko_wrong_unit(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("2.4 ton")
        self.assertFalse(valid)

    def test_ko_comma(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("2,4")
        self.assertFalse(valid)

    def test_ko_nospaceand_unit(self):
        mxw = MaxWeight()
        (valid, msg) = mxw.valid("200kg")
        self.assertFalse(valid)

