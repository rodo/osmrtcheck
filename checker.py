#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""
import os.path
import sqlite3
from imposm.parser import OSMParser
import sys
import re
from plugins.MaxWeight import MaxWeight
from plugins.MaxHeight import MaxHeight


class Checker(object):

    verbose = False
    maxs = ['maxweight', 'maxheight', 'maxspeed', 'maxwidth', 'maxaxleload',
            'maxage', 'maxlength', 'maxstay', 'maxdraft', 'maxtents',
            'maxdepth', 'maxdraught',
            'maxgcweight','maxweightrating',
            'maxgcweightrating', 'maxgvweightrating',]

    rules = {'maxweight': MaxWeight,
             'maxheight': MaxHeight}

    wrong_ways = []
    wrong_nodes = []
    url = 'http://www.openstreetmap.org'

    dbname = None

    nb_nodes = 0
    nb_ways = 0

    def checktag(self, nrw, tag, value):
        """Check the tag

        - nrw (string): node, way, relation
        - tag (string)
        - value (string)
        """
        msg = None
        result = True
        if tag.startswith('max'):
            prim = tag.split(':')[0]
            if not prim in self.maxs:
                msg = "wrong key '{}' ({})".format(prim, tag)
                result = False
            else:
                if tag in self.rules:
                    (result, msg) = getattr(self.rules[tag], "valid")(value)
                else:
                    result = True

        return (result, msg)

    def loadkeys(self):
        """Load keys from files
        """
        keys = []
        import osmkeys.regular
        keys.append(osmkeys.regular.keys)

    def nodes(self, nodes):
        """Check the nodes
        """
        for osmid, tags, ref in nodes:
            for tag in tags:
                (valid, msg) = self.checktag("node", tag, tags[tag])
                if not valid:
                    self.wrong_nodes.append(str(osmid))
                    self.dbstore(0, 'node', osmid, tag, tags[tag])
                    print "KO node  {} {}".format(osmid, msg)
                    print "   {}/node/{}".format(self.url, osmid)
                else:
                    if self.verbose:
                        print "OK node  {} {}".format(osmid, tag)

    def ways(self, ways):
        """Check the ways
        """
        for osmid, tags, ref in ways:
            for tag in tags:
                (valid, msg) = self.checktag("way", tag, tags[tag])
                if not valid:
                    self.wrong_ways.append(str(osmid))
                    self.dbstore(0, 'way', osmid, tag, tags[tag])
                    print "KO way  {} {}".format(osmid, msg)
                    print "   {}/way/{}".format(self.url, osmid)
                else:
                    if self.verbose:
                        print "OK way  {} {}={}".format(osmid, tag, tags[tag])

    def wways(self):
        """Return an array of way
        """
        data = []
        for way in self.wrong_ways:
            data.append("w{}".format(way))
        return data


    def dbstore(self, sequence, objecttype, osmid, key, value):
        """Store information in a sqlite db
        """
        conn = sqlite3.connect(self.dbname)
        c = conn.cursor()

        query = """INSERT INTO errors (date, sequence, osmid, objecttype, key, value)"""
        query = query + "VALUES (datetime('now'), {}, {}, '{}', '{}', '{}')".format(sequence,
                                                                                    osmid,
                                                                                    objecttype,
                                                                                    key,
                                                                                    value)

        c.execute(query)
        conn.commit()
        conn.close()


def createdb(fpath):
    """Create and initialize a sqlite database
    """
    conn = sqlite3.connect(fpath)
    c = conn.cursor()

    # Create table
    c.execute('''CREATE TABLE errors
             (date text, sequence integer, osmid integer, objecttype text, key text, value text)''')

    # Save (commit) the changes
    conn.commit()

    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()


def main(fpath):
    """The main function
    """

    dbname = "osmrtcheck.sqlite"

    if not os.path.exists(dbname):
        createdb(dbname)

    # instantiate counter and parser and start parsing
    chker = Checker()
    chker.dbname = dbname
    p = OSMParser(concurrency=4,
                  ways_callback=chker.ways,
                  nodes_callback=chker.nodes)

    # jsom Remote control
    josm = "http://127.0.0.1:8111/load_object?objects={}&new_layer=true"

    # Read filename on cmd arg
    p.parse(fpath)

    if len(chker.wrong_ways):
        print "ways {}".format(",".join(chker.wrong_ways))
        print josm.format(",".join(chker.wways()))
    if len(chker.wrong_nodes):
        print "nodes {}".format(",".join(chker.wrong_nodes))


if __name__ == '__main__':
    main(sys.argv[1])
