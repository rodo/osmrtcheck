--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: geobject; Type: TYPE; Schema: public; Owner: rodo
--

CREATE TYPE geobject AS ENUM (
    'node',
    'way',
    'relation'
);


ALTER TYPE public.geobject OWNER TO rodo;

--
-- Name: day_node_compute(); Type: FUNCTION; Schema: public; Owner: rodo
--

CREATE FUNCTION day_node_compute() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

	DELETE FROM day_nodes WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')  ;

	INSERT INTO day_nodes
	       SELECT date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
	       sum(modified), sum(created), sum(deleted) FROM nodes 
	       WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
	       GROUP BY date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

	RETURN NEW;

END;
$$;


ALTER FUNCTION public.day_node_compute() OWNER TO rodo;

--
-- Name: day_relation_compute(); Type: FUNCTION; Schema: public; Owner: rodo
--

CREATE FUNCTION day_relation_compute() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

	DELETE FROM day_relations WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')  ;

	INSERT INTO day_relations
	       SELECT date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
	       sum(modified), sum(created), sum(deleted) FROM relations 
	       WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
	       GROUP BY date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

	RETURN NEW;

END;
$$;


ALTER FUNCTION public.day_relation_compute() OWNER TO rodo;

--
-- Name: day_tag_compute(); Type: FUNCTION; Schema: public; Owner: rodo
--

CREATE FUNCTION day_tag_compute() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

        DELETE FROM day_tags WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')
           AND objects = NEW.objects AND tag = NEW.tag  ;

        INSERT INTO day_tags
               SELECT NEW.objects, date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
               tag, sum(number) FROM tags
               WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
               AND objects = NEW.objects AND tag = NEW.tag
               GROUP BY NEW.objects, tag, date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

        RETURN NEW;

END;
$$;


ALTER FUNCTION public.day_tag_compute() OWNER TO rodo;

--
-- Name: day_way_compute(); Type: FUNCTION; Schema: public; Owner: rodo
--

CREATE FUNCTION day_way_compute() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

	DELETE FROM day_ways WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')  ;

	INSERT INTO day_ways
	       SELECT date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
	       sum(modified), sum(created), sum(deleted) FROM ways 
	       WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
	       GROUP BY date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

	RETURN NEW;

END;
$$;


ALTER FUNCTION public.day_way_compute() OWNER TO rodo;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: day_nodes; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE day_nodes (
    date date,
    modified integer,
    created integer,
    deleted integer
);


ALTER TABLE public.day_nodes OWNER TO rodo;

--
-- Name: day_relations; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE day_relations (
    date date,
    modified integer,
    created integer,
    deleted integer
);


ALTER TABLE public.day_relations OWNER TO rodo;

--
-- Name: day_tags; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE day_tags (
    objects geobject,
    date date,
    tag text,
    number integer
);


ALTER TABLE public.day_tags OWNER TO rodo;

--
-- Name: day_ways; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE day_ways (
    date date,
    modified integer,
    created integer,
    deleted integer
);


ALTER TABLE public.day_ways OWNER TO rodo;

--
-- Name: nodes; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE nodes (
    date integer,
    sequence integer,
    modified integer,
    created integer,
    deleted integer,
    users integer,
    tags text
);


ALTER TABLE public.nodes OWNER TO rodo;

--
-- Name: relations; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE relations (
    date integer,
    sequence integer,
    modified integer,
    created integer,
    deleted integer,
    users integer,
    tags text
);


ALTER TABLE public.relations OWNER TO rodo;

--
-- Name: smallnodes; Type: VIEW; Schema: public; Owner: rodo
--

CREATE VIEW smallnodes AS
    SELECT nodes.date, nodes.sequence, nodes.modified, nodes.created, nodes.deleted, nodes.users, length(nodes.tags) AS length FROM nodes ORDER BY nodes.sequence DESC;


ALTER TABLE public.smallnodes OWNER TO rodo;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE tags (
    objects geobject,
    date integer,
    sequence integer,
    tag text,
    number integer
);


ALTER TABLE public.tags OWNER TO rodo;

--
-- Name: ways; Type: TABLE; Schema: public; Owner: rodo; Tablespace: 
--

CREATE TABLE ways (
    date integer,
    sequence integer,
    modified integer,
    created integer,
    deleted integer,
    users integer,
    tags text
);


ALTER TABLE public.ways OWNER TO rodo;

--
-- Name: day_nodes_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE UNIQUE INDEX day_nodes_date_idx ON day_nodes USING btree (date);

ALTER TABLE day_nodes CLUSTER ON day_nodes_date_idx;


--
-- Name: day_relations_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE UNIQUE INDEX day_relations_date_idx ON day_relations USING btree (date);


--
-- Name: day_tags_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX day_tags_date_idx ON day_tags USING btree (date);


--
-- Name: day_tags_tag_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX day_tags_tag_idx ON day_tags USING btree (tag);


--
-- Name: day_ways_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE UNIQUE INDEX day_ways_date_idx ON day_ways USING btree (date);


--
-- Name: nodes_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE UNIQUE INDEX nodes_date_idx ON nodes USING btree (date);


--
-- Name: nodes_relations_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX nodes_relations_idx ON relations USING btree (sequence);


--
-- Name: nodes_sequence_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX nodes_sequence_idx ON nodes USING btree (sequence);


--
-- Name: nodes_ways_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX nodes_ways_idx ON ways USING btree (sequence);


--
-- Name: relations_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE UNIQUE INDEX relations_date_idx ON relations USING btree (date);


--
-- Name: tags_sequence_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX tags_sequence_idx ON tags USING btree (sequence);


--
-- Name: tags_tag_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE INDEX tags_tag_idx ON tags USING btree (tag);


--
-- Name: ways_date_idx; Type: INDEX; Schema: public; Owner: rodo; Tablespace: 
--

CREATE UNIQUE INDEX ways_date_idx ON ways USING btree (date);


--
-- Name: day_node_compute; Type: TRIGGER; Schema: public; Owner: rodo
--

CREATE TRIGGER day_node_compute AFTER INSERT ON nodes FOR EACH ROW EXECUTE PROCEDURE day_node_compute();


--
-- Name: day_relation_compute; Type: TRIGGER; Schema: public; Owner: rodo
--

CREATE TRIGGER day_relation_compute AFTER INSERT ON relations FOR EACH ROW EXECUTE PROCEDURE day_relation_compute();


--
-- Name: day_tag_compute; Type: TRIGGER; Schema: public; Owner: rodo
--

CREATE TRIGGER day_tag_compute AFTER INSERT ON tags FOR EACH ROW EXECUTE PROCEDURE day_tag_compute();


--
-- Name: day_way_compute; Type: TRIGGER; Schema: public; Owner: rodo
--

CREATE TRIGGER day_way_compute AFTER INSERT ON ways FOR EACH ROW EXECUTE PROCEDURE day_way_compute();


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: day_nodes; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE day_nodes FROM PUBLIC;
REVOKE ALL ON TABLE day_nodes FROM rodo;
GRANT ALL ON TABLE day_nodes TO rodo;
GRANT SELECT ON TABLE day_nodes TO "www-data";


--
-- Name: day_relations; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE day_relations FROM PUBLIC;
REVOKE ALL ON TABLE day_relations FROM rodo;
GRANT ALL ON TABLE day_relations TO rodo;
GRANT SELECT ON TABLE day_relations TO "www-data";


--
-- Name: day_tags; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE day_tags FROM PUBLIC;
REVOKE ALL ON TABLE day_tags FROM rodo;
GRANT ALL ON TABLE day_tags TO rodo;
GRANT SELECT ON TABLE day_tags TO "www-data";


--
-- Name: day_ways; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE day_ways FROM PUBLIC;
REVOKE ALL ON TABLE day_ways FROM rodo;
GRANT ALL ON TABLE day_ways TO rodo;
GRANT SELECT ON TABLE day_ways TO "www-data";


--
-- Name: nodes; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE nodes FROM PUBLIC;
REVOKE ALL ON TABLE nodes FROM rodo;
GRANT ALL ON TABLE nodes TO rodo;
GRANT SELECT ON TABLE nodes TO "www-data";


--
-- Name: relations; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE relations FROM PUBLIC;
REVOKE ALL ON TABLE relations FROM rodo;
GRANT ALL ON TABLE relations TO rodo;
GRANT SELECT ON TABLE relations TO "www-data";


--
-- Name: tags; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE tags FROM PUBLIC;
REVOKE ALL ON TABLE tags FROM rodo;
GRANT ALL ON TABLE tags TO rodo;
GRANT SELECT ON TABLE tags TO "www-data";


--
-- Name: ways; Type: ACL; Schema: public; Owner: rodo
--

REVOKE ALL ON TABLE ways FROM PUBLIC;
REVOKE ALL ON TABLE ways FROM rodo;
GRANT ALL ON TABLE ways TO rodo;
GRANT SELECT ON TABLE ways TO "www-data";


--
-- PostgreSQL database dump complete
--

