#!/usr/bin/python

import psycopg2
import sys

def main(dbname, limit=20):

    try:
        conn = psycopg2.connect("dbname={}".format(dbname))
        cur = conn.cursor()
    except:
        print "Unable to connect to database {}".format(dbname)

    sqlmin = 'SELECT min(sequence) FROM nodes'
    sqlmax = 'SELECT max(sequence) FROM nodes'

    res = cur.execute(sqlmin)
    row = cur.fetchone()
    seqmin = row[0]

    res = cur.execute(sqlmax)
    row = cur.fetchone()
    seqmax = row[0]

    seq = seqmax
    missing = 0

    print "min/max {} {}".format(seqmin, seqmax)

    while (missing < limit):
        seq = seq - 1

        sql = 'SELECT count(sequence) FROM nodes WHERE sequence = %s'

        res = cur.execute(sql, (seq, ))
        row = cur.fetchone()
        if row[0] == 0:
            print "{} is missing".format(seq)
            missing += 1

    conn.close()

if __name__ == "__main__":

    dbname = 'oscstats'

    if len(sys.argv) > 1:
        main(dbname, int(sys.argv[1]))
    else:
        main(dbname)
