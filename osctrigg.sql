SET client_min_messages = warning;


DROP FUNCTION IF EXISTS day_node_compute() CASCADE;

CREATE FUNCTION day_node_compute() RETURNS trigger AS $day_node_compute$
BEGIN

	DELETE FROM day_nodes WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')  ;

	INSERT INTO day_nodes
	       SELECT date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
	       sum(modified), sum(created), sum(deleted) FROM nodes 
	       WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
	       GROUP BY date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

	RETURN NEW;

END;
$day_node_compute$ LANGUAGE plpgsql;

CREATE TRIGGER day_node_compute AFTER INSERT ON nodes
       FOR EACH ROW EXECUTE PROCEDURE day_node_compute();


DROP FUNCTION IF EXISTS day_way_compute() CASCADE;

CREATE FUNCTION day_way_compute() RETURNS trigger AS $day_way_compute$
BEGIN

	DELETE FROM day_ways WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')  ;

	INSERT INTO day_ways
	       SELECT date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
	       sum(modified), sum(created), sum(deleted) FROM ways 
	       WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
	       GROUP BY date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

	RETURN NEW;

END;
$day_way_compute$ LANGUAGE plpgsql;

CREATE TRIGGER day_way_compute AFTER INSERT ON ways
       FOR EACH ROW EXECUTE PROCEDURE day_way_compute();


DROP FUNCTION IF EXISTS day_relation_compute() CASCADE;

CREATE FUNCTION day_relation_compute() RETURNS trigger AS $day_relation_compute$
BEGIN

	DELETE FROM day_relations WHERE date = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second')  ;

	INSERT INTO day_relations
	       SELECT date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second'),
	       sum(modified), sum(created), sum(deleted) FROM relations 
	       WHERE date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + NEW.date * INTERVAL '1 second') = date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second')
	       GROUP BY date_trunc('day', TIMESTAMP WITH TIME ZONE 'epoch' + date * INTERVAL '1 second') ;

	RETURN NEW;

END;
$day_relation_compute$ LANGUAGE plpgsql;

CREATE TRIGGER day_relation_compute AFTER INSERT ON relations
       FOR EACH ROW EXECUTE PROCEDURE day_relation_compute();

