#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""
#
import re
import logging
import logging.handlers
import math
import util
from shapely.geometry import asShape
from shapely import wkt


class Alert(object):
    """An alert object
    """
    json = None
    geozone = None
    geozone_id = None
    geozone_json = None

    def __init__(self, json):
        self.cached = False
        self.id = json['id']
        self.json = json
        self.geozone_id = int(json['geozone'])

    def geozone_fromjson(self, json):
        """Load the geozone of alert

        json (dict)
        """
        self.geozone_json = json


class Rule(object):
    """A rule object
    """
    json = None
    alerts = None
    domains = None

    def __init__(self, json):
        self.json = json
        self.domains = json['domains']
        self.id = json['id']

    def nb_alerts(self):
        return len(self.alerts)


class ruleChecker(object):

    logger = None
    alerts = []
    action = None
    element = None

    matchrules = {}

    def __init__(self, frontend_url, frontend_token, **kwargs):
        self.init_logger(**kwargs)

        self.logger.debug('url: {} token {}'.format(frontend_url,
                                                    frontend_token))

        self.frontend_url = frontend_url
        self.frontend_token = frontend_token
        self.cache_geozones = {}
        self.cache_alerts = {}
        self.rules = {}
        self.nb_match = 0

        if 'action' in kwargs.keys():
            self.action = kwargs['action']

        if 'element' in kwargs.keys():
            self.element = kwargs['element']

    def stop(self):
        """Stop
        """
        self.logger.info('%d alerts in cache' % (len(self.cache_alerts)))
        self.logger.info('%d rules' % (len(self.rules)))
        self.logger.info('stop gracefully')

    def init_logger(self, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger(__name__)

        if 'loglevel' in kwargs:
            self.logger.setLevel(kwargs['loglevel'])
        else:
            self.logger.setLevel(logging.CRITICAL)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def fetch_alerts(self, domainids):
        """Fetch all active alerts from API

        Fetch alerts from API and return an Array(Alert)
        """
        content = None
        authtoken = "Token {}".format(self.frontend_token)
        headers = {'Authorization': authtoken}
        alerts = []

        for domainid in domainids:

            params = {'format': 'json',
                      'domain': domainid}

            url = "{}/alerts/".format(self.frontend_url)
            self.logger.debug("load alert %s" % (url))
            (status_code, content) = util.jsonalerts(url, params, headers)
            self.logger.debug('{} {}'.format(status_code, url))
            if status_code == 200:
                for alert in content:
                    new_alert = Alert(alert)
                    geoz = self.get_geozone(alert['geozone'])
                    new_alert.geozone_fromjson(geoz)
                    alerts.append(new_alert)

        return alerts

    def get_alerts(self, domainids):
        """Return the alert

        Lookup in the local cache if the alert was already loaded
        """
        key = "_".join(map(str, domainids))
        if key in self.cache_alerts:
            return self.cache_alerts[key]
        else:
            calerts = []
            alerts = self.fetch_alerts(domainids)
            for alert in alerts:
                alert.cached = True
                calerts.append(alert)
            self.cache_alerts[key] = calerts
            return alerts

    def get_geozone(self, zoneid):
        """Return the geozone

        Lookup in the local cache if the geozone was already loaded
        """
        key = str(zoneid)
        if key in self.cache_geozones:
            return self.cache_geozones[key]
        else:
            self.cache_geozones[key] = self.fetch_geozone(key)
            return self.cache_geozones[key]

    def fetch_geozone(self, zoneid):
        """Call geozone API
        """
        zone = None
        authtoken = "Token {}".format(self.frontend_token)
        headers = {'Authorization': authtoken}

        url = "{}/alert/zone/{}/data.geojson".format(self.frontend_url, zoneid)

        self.logger.debug("load geozone %s" % (url))
        (status_code, content) = util.jsonzone(url, None, headers)

        if status_code == 200:
            zone = content

        return zone

    def get_rules(self, action, element):
        """Get the rules
        """
        key = '{}_{}'.format(action, element)
        if key in self.rules.keys():
            return self.rules[key]
        else:
            (status, rules) = self.fetch_rules(action, element)
            if status == 200:
                self.rules[key] = rules
                return self.rules[key]
            else:
                return []

    def fetch_rules(self, action, element):
        """Fetch rules on web frontend

        - action (string) : 'create', 'modify'
        - element (string) : 'node', 'way', 'relation', 'all'
        """
        content = None
        authtoken = "Token {}".format(self.frontend_token)
        headers = {'Authorization': authtoken}

        data = {'format': 'json'}

        url = "{}/rules/{}/{}/".format(self.frontend_url,
                                       element,
                                       action)
        self.logger.debug("load rules %s" % (url))
        (status_code, content) = util.jsonrules(url, data, headers)

        return (status_code, content)

    def apply_geo_node(self, data, prev):
        """Look for change on a node

        - data : (string)
        - prev : (string)

        Return : (boolean)
        """
        old_geom = wkt.loads(data)
        old_geom.srid = 4326
        new_geom = wkt.loads(prev)
        new_geom.srid = 4326

        if old_geom.equals(new_geom):
            result = {'change': False}
        else:
            distance = old_geom.distance(new_geom)
            result = {'change': True,
                      'distance': distance,
                      'previous': prev,
                      'new': data,
                      'action': 'move'}

        return result

    def apply_tag_rules(self, datas, action, element):
        """Apply all the rules on a tag list

        - datas : json array [{"amenity": "fire_hydrant"}]
        - action : create/modify/delete
        - element : node/way/relation
        """
        matches = []
        tags = datas['tags']

        rules = self.get_rules(action, element)

        for tag in tags:

            value = "%s=%s" % (tag.keys()[0], tag.values()[0])

            for rule in rules:
                search = re.match(rule['tag_regex'], value)
                if search is not None:
                    ruleid = rule['id']
                    # print "%d matched" % ruleid
                    if ruleid in self.matchrules.keys():
                        newr = self.matchrules[ruleid]
                    else:
                        newr = Rule(rule)
                        newr.alerts = self.get_alerts(newr.domains)
                        self.matchrules[ruleid] = newr

                    alertids = self.isin_zone(newr, datas)
                    if len(alertids):
                        matches.append((rule['id'], alertids))
                        self.nb_match = self.nb_match + 1

        return matches

    def is_followed(self, data, action, element):
        """Check all rules and geozones

        - data (dict) : {"amenity": "fire_hydrant"}
        - action (string) : create/modify/delete
        - element (string) : node/way/relation

        Return : list
        """
        matches = []

        rules = self.get_rules(action, element)

        for rule in rules:
            newr = Rule(rule)
            # load alerts in object rule
            newr.alerts = self.get_alerts(newr.domains)

            alertids = self.isin_zone(newr, data)
            for alert in alertids:
                matches.append(alert)

        return list(set(matches))

    def isin_zone(self, rule, element):
        """
        - rule (Rule) : the rule
        - element : osm object
        {'changeset': '21574342', 'uid': '1507370',
         'tags': [{'name': u'Csibr\xe1k'}, {'place': 'village'}],
         'timestamp': '2014-04-08T16:37:25Z',
         'lon': 18.3426997, 'lat': 46.4658688,
         'version': '1', 'geom': 'POINT(18.3426997 46.4658688)',
         'user': 'Cpt Balu', 'action': 'create', 'type': 'node',
         'id': '2777484194'}

        Return:
         - (array) all alert id that cover this element
        """
        alertids = []

        for alert in rule.alerts:
            try:
                geom = wkt.loads(element['geom'])
            except:
                geom = None
            feature = alert.geozone_json['features']
            geozone = feature[0]['geometry']
            shape = asShape(geozone)
            if geom:
                if shape.contains(geom):
                    alertids.append(alert.id)

        return alertids


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat / 2) ** 2 + math.cos(lat1)
    b = math.cos(lat2) * math.sin(dlon / 2) ** 2
    c = 2 * math.asin(math.sqrt(a * b))
    km = 6367 * c
    return km
