#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""
import unittest
from mock import Mock, patch
from osmstorage import osmStorage
import logging


CONF = {
    'srid': 4326,
    'loglevel': 'INFO',
    'frontend_url': 'http://192.168.17.10:8000',
    'frontend_token': '0de7ba1a03e036c28ef2cab19ae2555d82e175ec',
    'osmrtcheck': {
        'dbname': 'osmrtcheck',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5432,
        },
    'osm2pgsql':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    'osmstat':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    }

NODE = """<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="OpenStreetMap server"
copyright="OpenStreetMap and contributors"
attribution="http://www.openstreetmap.org/copyright"
license="http://opendatacommons.org/licenses/odbl/1-0/">
  <node id="268016806" changeset="18627" timestamp="2008-06-02T08:57:33Z"
  version="1" visible="true" user="Pascal Courtonne" uid="20530"
  lat="49.0243146" lon="1.1497621">
    <tag k="created_by" v="JOSM"/>
  </node>
  <node id="268016806" changeset="326143" timestamp="2009-04-08T12:57:12Z"
  version="2" visible="true" user="Pascal Courtonne" uid="20530"
  lat="49.0242746" lon="1.1498057">
    <tag k="created_by" v="JOSM"/>
  </node>
  <node id="268016806" changeset="5970056" timestamp="2010-10-06T14:58:50Z"
  version="3" visible="true" user="chene" uid="198366"
  lat="49.0242831" lon="1.149797"/>
  <node id="268016806" changeset="27106367" timestamp="2014-11-29T08:30:03Z"
  version="4" visible="false" user="chene" uid="198366"/>
</osm>"""


class fakecursor(object):

    datas = ((1, 1, "un", "one"),
             (2, "deux", "two"))

    def execute(self, qry, parm=None):
        return None

    def fetchone(self):
        return self.datas[0]

    def fetchall(self):
        return self.datas

    def close(self):
        pass


class MockPg(object):
    def __init__(self, dsn):
        pass

    def set_isolation_level(self, level):
        pass

    def cursor(self):
        return fakecursor()

    def commit(self):
        pass


@patch('psycopg2.connect', MockPg)
class osmStorageTests(unittest.TestCase):

    def test_init(self):
        """
        """
        conf = {
            'osmrtcheck': {
                'dbname': 'osmrtcheck',
                'dbuser': 'rodo',
                'dbpass': 'rodo',
                'dbhost': 'localhost',
                'dbport': 5432,
                'srid': 4326,
                'loglevel': 'INFO',
                'frontend_url': 'http://192.168.17.10:8000',
                'frontend_token': '0de7ba1a03e036c28ef2cab19ae2555d82e175ec'
                },
            'osm2pgsql':  {
                'dbname': 'osmalsace',
                'dbuser': 'rodo',
                'dbpass': 'rodo',
                'dbhost': 'localhost',
                'dbport': 5433,
                },
            }

        res = osmStorage(conf)
        self.assertEqual(res.conf, conf)
        self.assertEqual(res.logger.getEffectiveLevel(), logging.CRITICAL)

    def test_init_loglevel(self):
        """Affect a non default loglevel
        """
        res = osmStorage(CONF, loglevel=logging.DEBUG)
        self.assertEqual(res.logger.getEffectiveLevel(), logging.DEBUG)

    def test_get_dsn(self):
        """The default datasource name
        """
        res = osmStorage(CONF, loglevel=logging.DEBUG)
        dsn = res.get_dsn()
        res = " ".join(["host='localhost'",
                        "dbname='osmrtcheck'",
                        "user='rodo'",
                        "password='rodo' port=5432"])
        self.assertEqual(dsn, res)

    def test_get_dsn_nondefault(self):
        """A non default data source name
        """
        res = osmStorage(CONF)
        dsn = res.get_dsn('osm2pgsql')
        res = " ".join(["host='localhost' dbname='osmalsace'",
                        "user='rodo' password='rodo' port=5433"])
        self.assertEqual(dsn, res)

    def test_seqpatch(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.seqpath("2410")
        self.assertEqual(res, "000/000/002")

    @patch('os.mkdir', Mock(return_value=True))
    @patch('os.path.isdir', Mock(return_value=True))
    def test_createdir(self):
        """
        """
        res = osmStorage(CONF, loglevel=logging.DEBUG)
        dsn = res.createdir('tmp/foo/debug')
        self.assertEqual(dsn, True)

    def test_getdsn(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.get_dsn()
        self.assertEqual(res, "".join("host='localhost' "
                                      "dbname='osmrtcheck' "
                                      "user='rodo' "
                                      "password='rodo' "
                                      "port=5432"))

    def test_check_node(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.check_node({'uid': 42,
                              'version': 1,
                              'user': 'Obi-Wan Kenobi'})

        att = {'uid': 42,
               'version': 1,
               'user': 'Obi-Wan Kenobi'}

        self.assertEqual(res, att)

    def test_check_node_missing(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.check_node({'version': 1})

        att = {'version': 1,
               'uid': 0,
               'user': 'anonymous'}

        self.assertEqual(res, att)

    def test_dblog_start(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.dblog_start("12", "dd")
        self.assertEqual(res, 1)

    def test_dblog_stop(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.dblog_stop(12)
        self.assertEqual(res, None)

    def test_store_stats(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.store_stats(((1, 'deleted', 'date', 'uid', 'alertid'),
                               (2, 'deleted', 'date', 'uid', 'alertid')))

        self.assertEqual(res, 2)

    def test_users_unknown(self):
        """
        """
        oss = osmStorage(CONF)
        res = oss.users_unknown()

        self.assertEqual(res, [1, 2])

    def test_store_changesets(self):
        """
        """
        oss = osmStorage(CONF)
        chg = {'1234': {'uid': 123, 'user': 'foo'}}
        res = oss.store_changesets(chg)
        self.assertEqual(res, None)

    def test_store_node(self):
        """
        """
        oss = osmStorage(CONF)
        node = {'id': 123, 'changeset': 456, 'uid': 987,
                'version': 1, 'lon': 2.5, 'lat': 44.2,
                'timestamp': '2014-03-04',
                'tags': [{'foo': 'bar'}]}
        res = oss.store_node(node)
        self.assertEqual(res, node)

    def test_get_previous_version(self):
        """
        """
        oss = osmStorage(CONF)
        chg = {'result': 'found',
               'changeset': 0,
               'tags': 1,
               'geom': 1}
        res = oss.get_previous_version('node', 12, 2)
        self.assertEqual(res, chg)

    @patch('osmdl.osmDownloader.fetch_node',
           Mock(return_value='node-history_sample.osm'))
    def test_fetch_previous_version(self):
        """
        """
        oss = osmStorage(CONF)
        prev = oss.fetch_previous_version('node', 12, 2)

        osm = [{'changeset': '18627', 'uid': '20530',
                'tags': [{'created_by': 'JOSM'}],
                'timestamp': '2008-06-02T08:57:33Z', 'lon': 1.1497621,
                'visible': True,
                'version': 1, 'user': 'Pascal Courtonne', 'lat': 49.0243146,
                'type': 'node', 'id': '268016806'},
               {'changeset': '326143', 'uid': '20530',
                'tags': [{'created_by': 'JOSM'}],
                'timestamp': '2009-04-08T12:57:12Z', 'lon': 1.1498057,
                'visible': True,
                'version': 2, 'user': 'Pascal Courtonne', 'lat': 49.0242746,
                'type': 'node', 'id': '268016806'},
               {'changeset': '5970056', 'uid': '198366', 'tags': [],
                'timestamp': '2010-10-06T14:58:50Z', 'lon': 1.149797,
                'visible': True,
                'version': 3, 'user': 'chene', 'lat': 49.0242831,
                'type': 'node',
                'id': '268016806'}]

        self.assertEqual(prev, osm)
        self.assertEqual(oss.osh.files_open, 1)
