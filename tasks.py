#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014,2021 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""
import json
import requests
from celery import Celery
from DictDiffer import DictDiffer

ACTION_CREATE = 1
ACTION_DELETE = 2
ACTION_TAGCHANGE = 3
ACTION_TAGDELETE = 4
ACTION_TAGCREATE = 5
ACTION_TOPOCHANGE = 6


app = Celery('tasks', broker='amqp://guest@localhost//')


@app.task
def node_create(conf, rule, node, alerts):
    """A new node is create

    - conf : (json) configuration options
    - rule : (integer) the rule which has matched
    - node : (json) the node
    """
    for alert in alerts:
        send_event(conf, rule, node, alert, ACTION_CREATE)

    return node


@app.task
def node_delete(conf, rule, node, alerts):
    """A node is delete

    - conf : (json) configuration options
    - rule : (integer) the rule which has matched
    - node : (json) the node
    """
    for alert in alerts:
        send_event(conf, rule, node, alert, ACTION_DELETE)

    return node


@app.task
def node_modify(conf, rule, node, previous, alerts, geomchange):
    """A node is modify

    - conf : (json) configuration options
    - rule : (integer) the rule which has matched
    - node : (json) the node

    node = {'changeset': '30000033', 'uid': '16881',
            'tags': [{'fire_hydrant:type': 'pillar'},
                     {'name': 'pillar'},
                     {'building': 'yes'}],
            'timestamp': '2014-02-24T12:43:09Z',
            'lon': 1.3, 'lat': 43.6,
            'version': '2',
            'geom': 'POINT(1.3 43.6)',
            'user': 'rodo', 'action': 'modify',
            'type': 'node', 'id': '321'}
            """

    tag_diff = DictDiffer(taglist(node['tags']), taglist(previous['tags']))
    diff = tag_diff.fulldiff()
    change = json.dumps({'tag_added': diff['added'],
                         'tag_removed': diff['removed'],
                         'tag_changed': diff['changed'],
                         'tag_unchanged': diff['unchanged'],
                         'geom_change': [geomchange]})

    if tag_diff.has_changes():
        action = ACTION_TAGCHANGE
    else:
        action = ACTION_TOPOCHANGE

    for alert in alerts:
        send_event(conf, rule, node, alert, action, change)

    return node


def taglist(value):
    """Convert an array of dict in a dict

    - Return dict
    """
    data_tags = {}
    for elem in value:
        data_tags[elem.keys()[0]] = elem.values()[0]
    return data_tags


def send_event(conf, rule, item, alertid, action, change=""):
    """Send the event to the web frontend

    - rule (integer)
    - item_type_id : 1 node, 2 way, 3 relation
    - alertid (integer)
    """
    dict_type = {'node': 1, 'way': 2, 'relation': 3}
    content = None
    #  authtoken = "Token {}".format(conf['frontend_token'])
    #  headers = {'Authorization': authtoken}
    headers = {'content-type': 'application/json'}

    url = "{}/api/v1/events/".format(conf['frontend_url'])

    data = {'alert': alertid,
            'rule': rule,
            'action': action,
            'item': dict_type[item['type']],
            'osmid': item['id'],
            'geom': item['geom'],
            'change': change,
            'changeset': item['changeset']}

    req = requests.post(url,
                        headers=headers,
                        data=json.dumps(data))

    if req.status_code == 201:
        content = req.content
    else:
        print (req.content)
        print (data)

    return (req.status_code, content)
