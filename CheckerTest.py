#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012,2014 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import syslog
import unittest
from mock import Mock
from checker import Checker


class CheckerTests(unittest.TestCase):

    def test_checker_wways(self):
        chk = Checker()
        chk.wrong_ways = ["123", "456"]
        self.assertEqual(["w123", "w456"], chk.wways())

    def test_checktag_nomax(self):
        chk = Checker()
        res = chk.checktag("node", "building", "yes")
        self.assertEqual((True, None), res)

    def test_checktag_max(self):
        """
        The tag begins with max
        """
        chk = Checker()
        res = chk.checktag("node", "maxage", "10")
        self.assertEqual((True, None), res)

    def test_checktag_maxweight_correct(self):
        """
        The tag begins with max
        """
        chk = Checker()
        res = chk.checktag("node", "maxweight", "10")
        self.assertEqual((True, None), res)

    def test_checktag_maxweight_wrong(self):
        """
        The tag begins with max
        """
        chk = Checker()
        res = chk.checktag("node", "maxweight", "foobar")
        self.assertEqual((False, "maxweight wrong format foobar"), res)
