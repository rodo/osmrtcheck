#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Download the diff datas
"""
from os import unlink, path
from os import environ, mkdir
import sys
import gzip
import requests
from optparse import OptionParser
import time


__version__ = "1.3.0"

PROTOCOLS = ['http', 'https']


class UnknownProtocolError(Exception):
    def __init__(self):
        self.value = 123


class osmReplicat(object):

    protocol = "http"
    host = "planet.openstreetmap.org"
    basedir = None

    def __init__(self, **kwargs):

        if 'host' in kwargs:
            self.host = kwargs['host']

        if 'basedir' in kwargs:
            self.basedir = kwargs['basedir']

        if 'protocol' in kwargs:
            if kwargs['protocol'] not in PROTOCOLS:
                raise UnknownProtocolError
            self.protocol = kwargs['protocol']

    def readheader(self):
        try:
            email = environ['DEBEMAIL']
            trouble = 'In case of trouble contact {}'.format(email)
            headers = {'User-Agent': trouble}
        except:
            headers = {'User-Agent': 'python/requests'}
        return headers

    def createdir(self, prefix, fpath):
        """Create directories where state and diff files will be stored

        prefix (string)
        fpath (string) : '000/234/456'
        """
        if not path.isdir(path.join(prefix, fpath[0:3])):
            mkdir(path.join(prefix, fpath[0:3]))

        if not path.isdir(path.join(prefix, fpath[0:3], fpath[4:7])):
            mkdir(path.join(prefix, fpath[0:3], fpath[4:7]))

        return path.join(prefix, fpath[0:3], fpath[4:7], fpath)

    def download(self, url, fpath, verbose=False):
        """Download a file

        url (string) : URL
        fpath (string) : file path where to store the file
        """
        result = None
        try:
            r = requests.get(url, timeout=10, headers=self.readheader())
            if r.status_code == 200:
                if verbose:
                    msg = "size %d\n" % (int(r.headers['content-length']))
                    sys.stdout.write(msg)
                f = open(fpath, 'wb')
                for chunk in r.iter_content(chunk_size=512 * 1024):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
                f.close()
            result = r.status_code
        except:
            pass
        return result, fpath

    def minute(self, sequ=None, replicat="minute"):
        """Retrieve the online status file
        """
        if sequ is None:
            url = "{}://{}/replication/{}/state.txt".format(self.protocol,
                                                            self.host,
                                                            replicat)
        else:
            url = "{}://{}/replication/{}/{}.state.txt".format(self.protocol,
                                                               self.host,
                                                               replicat,
                                                               sequ)

        (status_code, content) = self.urlfetch(url)
        if status_code == 200:
            datas = self.parsestate(content)
            return datas['sequenceNumber'], datas['timestamp']
        else:
            sys.stderr.write("{}\n".format(status_code))
            return None, None

    def urlfetch(self, url):
        """Fetch an url

        :param url: the url to fetch
        """
        status_code = -1
        datas = None
        try:
            r = requests.get(url, timeout=10, headers=self.readheader())
            status_code = r.status_code
            if r.status_code == 200:
                datas = r.text
        except:
            sys.stderr.write("{} failed\n".format(url))
            pass

        return (status_code, datas)

    def parsestate(self, data):
        """Parse a state file

        data (string)
        """
        datas = {}
        for line in data.split('\n'):
            if not line.startswith('#') and len(line):
                (key, value) = line.split('=')
                datas[key] = value

        return datas

    def sequenceUrl(self, sequenceNumber, replicat="minute"):
        """
        http://planet.openstreetmap.org/replication/minute/000/648/363.osc.gz
        """
        num = "%09d" % (int(sequenceNumber))
        path = "{}/{}/{}".format(num[0:3], num[3:6], num[6:9])

        return "{}://{}/replication/{}/{}.osc.gz".format(self.protocol,
                                                         self.host,
                                                         replicat,
                                                         path)

    def oscname(self, sequenceNumber):
        """Return the filename

        Params :
        - sequenceNumber (integer) : the sequence number

        Return :
        - (string) the file path
        """
        num = "%09d" % (int(sequenceNumber))
        path = "{}.osc.gz".format(self.seqpath(num))

        return path

    def storename(self, sequenceNumber, prefix):
        """Return the filepath

        Params :
        - sequenceNumber (integer) : the sequence number
        - prefix (string) : directory

        Return :
        - (string) the file path
        """
        dirpath = path.join(prefix, self.oscname(sequenceNumber))

        return dirpath

    def statepath(self, sequenceNumber):
        """Return the state file path

        :param sequenceNumber: the sequence number `int`
        """
        path = "{}.state.txt".format(self.seqpath(sequenceNumber))

        return path

    def seqpath(self, sequenceNumber):
        """Build the path from sequence number

        """
        num = "%09d" % (int(sequenceNumber))
        path = "{}/{}/{}".format(num[0:3], num[3:6], num[6:9])

        return path

    def clean(self, sequenceNumber):
        """Remove old files

        Sometimes files are missed, so we ugly loop 10 times
        """
        i = 0
        while i < 10:
            fpath = self.oscname(int(sequenceNumber) - (10 + i))
            i = i + 1
            try:
                unlink(fpath)
            except:
                pass

    def uncompress(self, fpath, lastfilename, sequence=None):
        """Uncompress the file
        """
        f = gzip.open(fpath, 'rb')
        file_content = f.read()
        f.close()

        if sequence is not None:
            newpath = "history.osc"
        else:
            newpath = lastfilename

        f = open(newpath, 'wb')
        f.write(file_content)
        f.close()
        return newpath

    def storeSequenceNumber(self, sequence, timestamp, prefix, last=None):
        """Store the state file

        Only store in sequence.txt when it's the last file

        last (boolean)
        """
        if last is None:
            f = open(self.filepath('sequence.txt'), 'w')
            f.write('{},{}'.format(sequence, timestamp))
            f.close()

        fpath = self.statepath(sequence)
        if self.basedir is not None:
            self.createdir(self.basedir, self.seqpath(sequence))
        f = open(path.join(prefix, fpath), 'w')
        f.write('{},{}'.format(sequence, timestamp))
        f.close()
        return path.join(prefix, fpath)

    def filepath(self, filename):
        """Build the file path depends on basedir"""
        if self.basedir is not None:
            return path.join(self.basedir, filename)
        else:
            return filename

    def main(self, opts):
        """Main function
        """
        fpath = None
        start = time.time()
        sequence = None
        last_min = None
        # instantiate counter and parser and start parsing
        if opts.sequence > 0:
            ssq = self.seqpath(opts.sequence)
            tpath = self.storename(opts.sequence, opts.prefix)
            if path.exists(tpath):
                return tpath
            last_min, timestamp = self.minute(ssq, opts.replicat)
        else:
            last_min, timestamp = self.minute(replicat=opts.replicat)
        if last_min is not None:
            self.createdir(opts.prefix, self.seqpath(last_min))
            self.storeSequenceNumber(last_min,
                                     timestamp,
                                     opts.prefix,
                                     sequence)
            url = self.sequenceUrl(last_min, opts.replicat)
            if opts.verbose > 0:
                sys.stdout.write("fetch url %s\n" % (url))
            fpath = self.storename(last_min, opts.prefix)
            result, fpath = self.download(url, fpath, opts.verbose)
            if result:
                resuco = self.uncompress(fpath, opts.lastfile, sequence)
                if opts.verbose:
                    sys.stdout.write("Write file %s\n" % (resuco))
            if opts.verbose:
                print ("took : %s seconds" % (time.time() - start))

        return fpath


def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-s SEQUENCE] [-v]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)

    parser.add_option("-d", "--dbname", dest="dbname",
                      help="database name",
                      default='oscstats')
    parser.add_option("--host", dest="host",
                      help="OSM API hostname",
                      type='string',
                      default='planet.openstreetmap.org')
    parser.add_option("-l", "--last-file", dest="lastfile",
                      help="name of lastfile (default lastdata.osc)",
                      type='string',
                      default='lastdata.osc')
    parser.add_option("-p", "--prefix", dest="prefix",
                      help="directory name prefix to store datas",
                      type='string',
                      default='./')
    parser.add_option("-r", "--replicat", dest="replicat",
                      help="replicat type",
                      type='string',
                      default='minute')
    parser.add_option("-s", "--sequence", dest="sequence",
                      type=int,
                      help="sequence number",
                      default=0)
    parser.add_option("-v", "--verbose", dest="verbose",
                      type=int,
                      help="sequence number",
                      default=0)

    return parser.parse_args()[0]


def main(options):
    osr = osmReplicat()
    fpath = osr.main(options)
    return fpath

if __name__ == '__main__':
    options = arg_parse()
    fpath = main(options)
    if options.verbose > 0:
        sys.stdout.write("%s\n" % (fpath))
    sys.exit(0)
