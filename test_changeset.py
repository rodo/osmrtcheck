#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""

import unittest
from mock import Mock, patch
from changeset import Changeset


CONF = {
    'srid': 4326,
    'loglevel': 'INFO',
    'frontend_url': 'http://192.168.17.10:8000',
    'frontend_token': '0de7ba1a03e036c28ef2cab19ae2555d82e175ec',
    'osmrtcheck': {
        'dbname': 'osmrtcheck',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    'osm2pgsql':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    'osmstat':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    }


@patch('changeset.Changeset.initclasses', Mock(return_value=True))
class changesetTests(unittest.TestCase):

    def test_init(self):
        """
        """
        res = Changeset(CONF)
        self.assertEqual(res.config, CONF)

    def test_cache_init(self):
        """
        """
        res = Changeset(CONF)
        self.assertEqual(res.cache_store, {})

    def test_cache(self):
        """
        """
        res = Changeset(CONF)
        res.cache({'osmid': 45, 'uid': 54})
        self.assertEqual(len(res.cache_store), 1)

    def test_cache_maxdefault(self):
        """
        """
        res = Changeset(CONF)
        res.cache_size = 3
        res.cache({'osmid': 41, 'uid': 54})
        res.cache({'osmid': 42, 'uid': 54})
        res.cache({'osmid': 43, 'uid': 54})
        res.cache({'osmid': 44, 'uid': 54})
        res.cache({'osmid': 45, 'uid': 54})
        res.cache({'osmid': 46, 'uid': 54})
        self.assertEqual(len(res.cache_store), 4)

    def test_get(self):
        """
        """
        res = Changeset(CONF)
        res.cache_size = 3
        res.cache({'osmid': 41, 'uid': 54})
        res.cache({'osmid': 42, 'uid': 54})
        res.cache({'osmid': 43, 'uid': 54})
        res.cache({'osmid': 44, 'uid': 54})
        res.cache({'osmid': 45, 'uid': 54})
        res.cache({'osmid': 46, 'uid': 54})
        self.assertEqual(res.get(42), {'osmid': 42, 'uid': 54})
