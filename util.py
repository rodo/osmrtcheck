import requests
import json


def jsonrequ(url, data, headers):
    """Process a HTTP request and return (status, content)
    """
    content = None
    req = requests.get(url,
                       params=data,
                       headers=headers)

    if req.status_code == 200:
        content = json.loads(req.content)

    return (req.status_code, content)


def jsonrules(url, data, headers):
    """Load rules from API

    Usefull for mock process in tests
    """
    return jsonrequ(url, data, headers)


def jsonalerts(url, data, headers):
    """Load alerts from API

    Usefull for mock process in tests
    """
    return jsonrequ(url, data, headers)


def jsonzone(url, data, headers):
    """Load zone from API

    Usefull for mock process in tests
    """
    return jsonrequ(url, data, headers)
