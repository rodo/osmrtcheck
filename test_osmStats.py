#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import logging
from osmStats import osmStats


class osmStatsTests(unittest.TestCase):

    def test_init(self):
        ost = osmStats()
        self.assertEqual(ost.store(), [])

    def test_init_nondefault(self):
        ost = osmStats(loglevel=logging.CRITICAL)
        self.assertEqual(ost.store(), [])

    def test_mkey(self):
        ost = osmStats()
        key = ost.mkey('2014-01-06T12:43:56Z', 1234, 7)
        self.assertEqual(key, '2014-01-06-7-1234')

    def test_node_delete(self):
        ost = osmStats()
        ost.node_delete('2014-01-06T12:43:56Z', 1234, 7)
        res = [(1, '2014-01-06', 1234, 7, 0, 0, 1, 1, 2014)]
        self.assertEqual(ost.store(), res)

    def test_node_create(self):
        ost = osmStats()
        res = ost.node_create('2014-01-06T12:43:56Z', 1234, 7)
        self.assertEqual(res, (1, '2014-01-06-7-1234'))

    def test_node_modify(self):
        ost = osmStats()
        ost.node_modify('2014-01-06T12:43:56Z', 1234, 7)
        res = [(1, '2014-01-06', 1234, 7, 0, 1, 0, 1, 2014)]
        self.assertEqual(ost.store(), res)

    def test_store(self):
        """Store statistics

        - delete 3 items
        - modify 1 item
        - create 2 items
        """
        ost = osmStats()
        ost.node_delete('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_delete('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_delete('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_modify('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_create('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_create('2014-01-06T12:43:56Z', 1234, 7)
        res = [(1, '2014-01-06', 1234, 7, 2, 1, 3, 1, 2014)]
        self.assertEqual(ost.store(), res)

    def test_store_array(self):
        """Store mixed statistics ways and nodes

        - delete 2 nodes
        - modify 1 node
        - create 2 nodes
        - delete 1 way
        """
        ost = osmStats()
        ost.node_delete('2014-01-06T12:43:56Z', 1234, 7)
        ost.way_delete('2014-01-06T12:43:56Z', 1234, 8)
        ost.node_delete('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_modify('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_create('2014-01-06T12:43:56Z', 1234, 7)
        ost.node_create('2014-01-06T12:43:56Z', 1234, 7)
        res = [(1, '2014-01-06', 1234, 7, 2, 1, 2, 1, 2014),
               (2, '2014-01-06', 1234, 8, 0, 0, 1, 1, 2014)]
        self.assertEqual(ost.store(), res)


suite = unittest.TestLoader().loadTestsFromTestCase(osmStatsTests)
unittest.TextTestRunner(verbosity=2).run(suite)
