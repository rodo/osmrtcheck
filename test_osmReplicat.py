#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012,2015 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import tempfile
import responses
import unittest
import osmreplicat
from requests.exceptions import ConnectionError
from httplib import BadStatusLine
from mock import Mock, patch

STATE = """#Wed Oct 01 20:23:03 UTC 2014
sequenceNumber=1072681
txnMaxQueried=599045232
txnReadyList=
timestamp=2014-10-01T20\:23\:02Z
txnMax=599045232
txnActiveList=
"""


class ReplicatTests(unittest.TestCase):

    def test_init_default(self):
        osr = osmreplicat.osmReplicat()
        self.assertEqual(osr.host, "planet.openstreetmap.org")
        self.assertEqual(osr.protocol, "http")
        self.assertEqual(osr.basedir, None)

    def test_init_nondefault(self):
        osr = osmreplicat.osmReplicat(host="foo.bar", protocol="https",
                                      basedir="/tmp")
        self.assertEqual(osr.host, "foo.bar")
        self.assertEqual(osr.protocol, "https")
        self.assertEqual(osr.basedir, "/tmp")

    def test_init_error(self):
        with self.assertRaises(osmreplicat.UnknownProtocolError) as cm:
            osmreplicat.osmReplicat(host="foo.bar", protocol="ftp")

        self.assertEqual(cm.exception.value, 123)

    def test_oscname(self):
        osr = osmreplicat.osmReplicat()
        self.assertEqual("000/001/201.osc.gz", osr.oscname("1201"))
        self.assertEqual("000/000/120.osc.gz", osr.oscname("120"))

    def test_sequenceUrl(self):
        osr = osmreplicat.osmReplicat(host="planet.org")
        url = "http://planet.org/replication/minute/000/648/363.osc.gz"
        self.assertEqual(url, osr.sequenceUrl("648363"))

    @patch('osmreplicat.osmReplicat.urlfetch', Mock(return_value=(200, STATE)))
    def test_minute(self):
        osr = osmreplicat.osmReplicat()
        res = osr.minute("http://foo")
        self.assertEqual(res, ('1072681', '2014-10-01T20\:23\:02Z'))

    @patch('osmreplicat.osmReplicat.urlfetch', Mock(return_value=(404, STATE)))
    def test_minute_failed(self):
        """Fail to retreive url
        """
        osr = osmreplicat.osmReplicat()
        res = osr.minute("http://foo")
        self.assertEqual(res, (None, None))

    @responses.activate
    def test_urlfetch(self):
        responses.add(**{
            'method': responses.GET,
            'url': 'http://foo.url',
            'body': '{"error": "reason"}',
            'status': 200,
            'content_type': 'application/json',
            'adding_headers': {'X-Foo': 'Bar'}
        })

        osr = osmreplicat.osmReplicat()
        res = osr.urlfetch('http://foo.url')
        self.assertEqual(res, (200, '{"error": "reason"}'))

    @responses.activate
    def test_urlfetch_404error(self):
        """The url return a 404 error"""
        responses.add(**{
            'method': responses.GET,
            'url': 'http://foo.url',
            'body': '{"error": "reason"}',
            'status': 404,
            'content_type': 'application/json',
            'adding_headers': {'X-Foo': 'Bar'}
        })

        osr = osmreplicat.osmReplicat()
        res = osr.urlfetch('http://foo.url')
        self.assertEqual(res, (404, None))

    @responses.activate
    def test_urlfetch_exception(self):
        """Aan exception occurs on fetch"""
        exception = ConnectionError('Connection aborted.',
                                    BadStatusLine("''",))
        responses.add(responses.GET, 'http://foo.url',
                      body=exception)

        osr = osmreplicat.osmReplicat()
        res = osr.urlfetch('http://foo.url')
        self.assertEqual(res, (-1, None))

    def test_parsestate(self):
        osr = osmreplicat.osmReplicat()
        res = osr.parsestate(STATE)
        self.assertEqual(res, {'sequenceNumber': '1072681',
                               'txnMaxQueried': '599045232',
                               'txnReadyList': '',
                               'timestamp': '2014-10-01T20\:23\:02Z',
                               'txnMax': '599045232',
                               'txnActiveList': ''})

    def test_statepath(self):
        osr = osmreplicat.osmReplicat()
        res = osr.statepath('1072681')
        self.assertEqual(res, '001/072/681.state.txt')

    def test_seqpath(self):
        osr = osmreplicat.osmReplicat()
        res = osr.seqpath('1072681')
        self.assertEqual(res, '001/072/681')

    @patch('os.mkdir', Mock(return_value=True))
    @patch('os.path.isdir', Mock(return_value=True))
    def test_createdir(self):
        osr = osmreplicat.osmReplicat()
        res = osr.createdir('/dev/null', '000/123/456')
        self.assertEqual(res, '/dev/null/000/123/000/123/456')

    def test_readheader(self):
        osr = osmreplicat.osmReplicat()
        res = osr.readheader()
        self.assertTrue('User-Agent' in res.keys())

    def test_uncompress(self):

        fpath = os.path.join(os.path.dirname(__file__),
                             'samples',
                             'node.osm.gz')

        node = os.path.join(os.path.dirname(__file__),
                            'samples',
                            'node.osm')

        tmp = tempfile.mkstemp()

        osr = osmreplicat.osmReplicat()
        res = osr.uncompress(fpath, tmp[1], None)
        self.assertEqual(res, tmp[1])
        self.assertEqual(open(tmp[1]).read(), open(node).read())

    def test_uncompress_seq(self):

        fpath = os.path.join(os.path.dirname(__file__),
                             'samples',
                             'node.osm.gz')
        tmp = tempfile.mkstemp()

        osr = osmreplicat.osmReplicat()
        res = osr.uncompress(fpath, tmp[1], 1234)
        self.assertEqual(res, 'history.osc')

    @responses.activate
    def test_download(self):
        responses.add(**{
            'method': responses.GET,
            'url': 'http://foo.url',
            'body': '{"error": "reason"}',
            'status': 200,
            'content_type': 'application/json',
            'adding_headers': {'X-Foo': 'Bar'}
        })

        tmp = tempfile.mkstemp()

        osr = osmreplicat.osmReplicat()
        res = osr.download('http://foo.url', tmp[1])
        self.assertEqual(res, (200, tmp[1]))
        result = open(tmp[1]).read()
        self.assertEqual(result, '{"error": "reason"}')

    def test_filepath(self):
        osr = osmreplicat.osmReplicat()
        res = osr.filepath('foo.txt')
        self.assertEqual(res, 'foo.txt')

    def test_filepath_nondefault(self):
        osr = osmreplicat.osmReplicat(basedir="/tmp/foo")
        res = osr.filepath('foo.txt')
        self.assertEqual(res, '/tmp/foo/foo.txt')

    def test_storeSequenceNumber(self):
        tmpdir = tempfile.mkdtemp()
        osr = osmreplicat.osmReplicat(basedir=tmpdir)
        res = osr.storeSequenceNumber(4353, '2014-12-15T20:12:02Z', tmpdir)
        self.assertTrue(os.path.isdir(tmpdir))
        self.assertEqual(res, os.path.join(tmpdir, '000','004','353.state.txt'))

    def test_storeSequenceNumberLast(self):
        tmpdir = tempfile.mkdtemp()
        osr = osmreplicat.osmReplicat(basedir=tmpdir)
        res = osr.storeSequenceNumber(4353, '2014-12-15T20:12:02Z', tmpdir, 'foo')
        self.assertTrue(os.path.isdir(tmpdir))
        self.assertEqual(res, os.path.join(tmpdir, '000','004','353.state.txt'))

