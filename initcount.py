#!/usr/bin/python

import sqlite3
import psycopg2


def sqlinit():

    conn = sqlite3.connect('osc_stats.sqlite')
    c = conn.cursor()

# Create table
    c.execute('''CREATE TABLE nodes
             (date text, sequence integer, modified integer, created integer, deleted integer)''')
    
# Create table
    c.execute('''CREATE TABLE ways
             (date text, sequence integer, modified integer, created integer, deleted integer)''')
    
# Create table
    c.execute('''CREATE TABLE relations
             (date text, sequence integer, modified integer, created integer, deleted integer)''')
    
# Save (commit) the changes
    conn.commit()
    
    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()


try:
    conn = psycopg2.connect("dbname='oscstats'")
except:
    print "I am unable to connect to the database"

cur = conn.cursor()

for table in ['nodes', 'ways', 'relations']:

    sql = "CREATE TABLE {} (date integer, sequence integer, modified integer, created integer, deleted integer)"

    try:
        cur.execute(sql.format(table))
    except:
        cur = conn.cursor()

    sql = "CREATE TABLE day_{} (date date, modified integer, created integer, deleted integer)"
    print sql.format(table)
    cur.execute(sql.format(table))


    sql = "CREATE UNIQUE INDEX {}_date_idx ON {} (date)"

    try:
        cur.execute(sql.format(table, table))
    except:
        pass

    sql = "CREATE UNIQUE INDEX day_{}_date_idx ON {} (date)"

    try:
        cur.execute(sql.format(table, table))
    except:
        pass


conn.commit()
conn.close()
