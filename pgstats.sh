#!/bin/sh

# Read info in pg_stat_statements
#
#
psql -At -d postgres -U jenkins -c "COPY (SELECT sum(calls),sum(rows) FROM pg_stat_statements) TO STDOUT WITH (FORMAT 'csv')"