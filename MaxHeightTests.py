#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2013 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Ref : http://wiki.openstreetmap.org/wiki/Key%3Amaxheight
#
import unittest

from checker import MaxHeight

class MaxHeightTests(unittest.TestCase):

    def test_ok_correct(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("2.4")
        self.assertTrue(valid)

    def test_ok_correct1(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("200")
        self.assertTrue(valid)

    def test_ok_correct2(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("0.4")
        self.assertTrue(valid)

    def test_ok_correct_meter_unit(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("2.4 m")
        self.assertTrue(valid)

    def test_ok_correct_inches_unit(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("2'4\"")
        self.assertTrue(valid)

    def test_ok_correct_inches_zero(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("2'0\"")
        self.assertTrue(valid)

    def test_ok_exclusion(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("FIXME")
        self.assertTrue(valid)

    def test_ko_negative_value(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("-2.4")
        self.assertFalse(valid)

    def test_ko_comma(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.valid("2,4")
        self.assertFalse(valid)
        attend = "maxheight wrong format {}".format("2,4")
        self.assertEqual(msg, attend)

    def test_ok_withunits(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.withunits("2'4\"")
        self.assertTrue(valid)

    def test_ko_withunits(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.withunits("2'")
        self.assertFalse(valid)

    def test_ok_floatvalue(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.floatvalue("2.0")
        self.assertTrue(valid)

    def test_ko_floatvalue_comma(self):
        mxh = MaxHeight()
        (valid, msg) = mxh.floatvalue("2,0")
        self.assertFalse(valid)
