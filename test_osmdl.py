#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import responses
from requests.exceptions import ConnectionError
from httplib import BadStatusLine
from osmdl import osmDownloader


class osmDownloaderTests(unittest.TestCase):

    @responses.activate
    def test_fetch_error(self):
        responses.add(**{
            'method': responses.GET,
            'url': 'http://foo.url',
            'body': '{"error": "reason"}',
            'status': 404,
            'content_type': 'application/json',
            'adding_headers': {'X-Foo': 'Bar'}
        })

        ost = osmDownloader()
        self.assertEqual(ost.fetch('http://foo.url', {}), '')

    @responses.activate
    def test_fetch_ok(self):
        responses.add(**{
            'method': responses.GET,
            'url': 'http://foo.url',
            'body': '{"error": "reason"}',
            'status': 200,
            'content_type': 'application/json',
            'adding_headers': {'X-Foo': 'Bar'}
        })

        ost = osmDownloader()
        res = ost.fetch('http://foo.url', {})
        self.assertEqual(res, '{"error": "reason"}')

    @responses.activate
    def test_fetch_badstatusline(self):
        exception = ConnectionError('Connection aborted.',
                                    BadStatusLine("''",))
        responses.add(responses.GET, 'http://foo.url',
                      body=exception)

        ost = osmDownloader(retry_after=0.1)
        res = ost.fetch('http://foo.url', {})
        self.assertEqual(res, None)
