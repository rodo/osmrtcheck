#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014,2021 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""
#
#
#
#
import sys
from optparse import OptionParser
import logging
import logging.handlers
from tasks import node_create, node_delete, node_modify
from oscparser.oscparser import oscParser
from rulechecker import ruleChecker
from osmstorage import osmStorage
from osmStats import osmStats
from osmelastic import osmElastic
import config
import time
from changeset import Changeset

__version__ = "1.0.1"


class ocheck(object):

    nb_nodes = 0
    nb_ways = 0
    logger = None

    def __init__(self, config, **kwargs):
        self.config = config

        try:
            loglevel = config['loglevel']
        except KeyError:
            loglevel = 'ERROR'

        self.init_logger(loglevel, **kwargs)

        pfile = '%s' % (kwargs['parsefile'])
        self.logger.info("START %s" % (pfile))

        self.rulc = ruleChecker(config['frontend_url'],
                                config['frontend_token'],
                                action='create',
                                element='node',
                                loglevel=loglevel)

        # instantiate a db
        self.dblog_id = 0
        self.store = osmStorage(config, loglevel=loglevel)
        self.changeset = Changeset(config, loglevel=loglevel)
        # instantiate elasticsearch
        self.elastic = osmElastic(config, loglevel=loglevel)

        self.dblog_id = self.store.dblog_start('ocheck', {"parsefile": pfile})
        # statistics counter
        self.stats = osmStats(loglevel=loglevel)

    def stop(self):
        self.store.stop()
        self.rulc.stop()
        self.logger.info("%s nodes, %s ways parsed" % (self.nb_nodes,
                                                       self.nb_ways))
        self.logger.info("ocheck stop normally")

    def init_logger(self, loglevel, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger("dispatcher")

        self.logger.setLevel(loglevel)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)
        
        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)


    def delete_trig(self, data):
        """Actions done on delete
        """
        previous = self.store.get_previous_version(data['type'],
                                                   data['id'],
                                                   int(data['version']))
        if previous['result'] == 'found':
            data['tags'] = previous['tags']
            # apply rules
            rules = self.rulc.apply_tag_rules(data, 'delete', 'node')

            for (rule, alerts) in rules:
                node_delete.delay(self.config, rule, data, alerts)

    def update_trig(self, data):
        """
        """
        # get the previous version
        previous = self.store.get_previous_version(data['type'],
                                                   data['id'],
                                                   int(data['version']))
        if previous['result'] == 'found':
            # apply rules
            tagrules = self.rulc.apply_tag_rules(data, 'modify', 'node')
            georules = self.rulc.apply_geo_node(data['geom'], previous['geom'])

            # store the new version
            if len(tagrules) > 0:
                self.store.store_node(data)

            for (rule, alerts) in tagrules:
                node_modify.delay(self.config,
                                  rule,
                                  data,
                                  previous,
                                  alerts,
                                  georules)

    def ccnode(self, data):
        """Node callback

        - call the right task depends on action

        data {'changeset': '20750033',
              'uid': '16881',
              'tags': [],
              'timestamp': '2014-02-24T12:43:09Z',
              'lon': 1.3,
              'lat': 45.6,
              'version': '1',
              'geom': 'POINT(1.3 45.6)',
              'user': 'rodo',
              'action': 'delete',
              'type': 'node',
              'id': '423'}
        """
        self.nb_nodes += 1
        chg = self.changeset.get(int(data['changeset']))
        store_in_es = False
        try:
            if '#hotosm' in chg['tags']['comment']:                            
                store_in_es = True
        except:
            pass
        if store_in_es:
            self.elastic.store_node(chg, data)

        if data['action'] == 'create':
            stats = []
            rules = self.rulc.apply_tag_rules(data, 'create', 'node')
            for (rule, alerts) in rules:
                for alert_id in alerts:
                    stats = self.stats.node_create(data['timestamp'],
                                                   data['uid'],
                                                   alert_id)
            # store the node in DB
            if len(rules) > 0:
                self.store.store_node(data)

            for (rule, alerts) in rules:
                #
                node_create.delay(self.config, rule, data, alerts)
            return (stats)

        if data['action'] == 'delete':
            trigs = []
            stats = []
            rules = self.rulc.is_followed(data, 'delete', 'node')
            for alert_id in rules:
                stats = self.stats.node_delete(data['timestamp'],
                                               data['uid'],
                                               alert_id)
            if len(rules) > 0:
                trigs = self.delete_trig(data)
            return (trigs, stats)

        if data['action'] == 'modify':
            trigs = []
            stats = []
            rules = self.rulc.is_followed(data, 'modify', 'node')
            for alert_id in rules:
                stats = self.stats.node_modify(data['timestamp'],
                                               data['uid'],
                                               alert_id)
            if len(rules) > 0:
                trigs = self.update_trig(data)
            return (trigs, stats)

    def ccway(self, data):
        """Way callback

        - call the right task depends on action

        data (dict)

        Return :
        """
        self.nb_ways += 1
        if data['action'] == 'create':
            rules = self.rulc.is_followed(data, 'create', 'way')
            for alert_id in rules:
                self.stats.way_create(data['timestamp'],
                                      data['uid'],
                                      alert_id)

        if data['action'] == 'delete':
            # get the previous version
            rules = self.rulc.is_followed(data, 'delete', 'way')
            for alert_id in rules:
                self.stats.way_delete(data['timestamp'],
                                      data['uid'],
                                      alert_id)

        if data['action'] == 'modify':
            rules = self.rulc.is_followed(data, 'modify', 'way')
            for alert_id in rules:
                self.stats.way_modify(data['timestamp'],
                                      data['uid'],
                                      alert_id)


def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-s SEQUENCE] [-v]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)

    parser.add_option("-i", "--input", dest="input",
                      type="string",
                      help="file name to parse",
                      default=None)

    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="be verbose",
                      default=False)

    return parser.parse_args()[0]


def main(opts):
    start = time.time()
    if opts.verbose:
        sys.stdout.write("Frontend : {}\n".format(config.conf['frontend_url']))
        sys.stdout.write("Parse file %s\n" % (opts.input))

    parsefile = opts.input

    # instantiate a dispatcher
    dispatch = ocheck(config.conf, parsefile=parsefile)

    # instantiate a parser
    osc = oscParser(config.conf, opts.verbose)
    osc.node_callback = dispatch.ccnode
    osc.way_callback = dispatch.ccway
    osc.parsefile([parsefile])
    dispatch.store.store_changesets(osc.changesets)

    # return statistics to store
    stats = dispatch.stats.store()
    # store statistics to database
    dispatch.store.store_stats(stats)

    if opts.verbose:
        print ("took %s seconds" % (time.time() - start))

    dispatch.store.dblog_stop(dispatch.dblog_id)
    dispatch.stop()

if __name__ == '__main__':
    options = arg_parse()
    if options.input is None:
        sys.stderr.write('Missing -i option\n')
        sys.exit(1)
    main(options)
