#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""

import unittest
from mock import Mock, patch
from oshparser import oshParser
import xml.etree.ElementTree as etree

XML = """<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="OpenStreetMap server" copyright="OpenStreetMap and contributors" attribution="http://www.openstreetmap.org/copyright" license="http://opendatacommons.org/licenses/odbl/1-0/">
  <way id="29270328" changeset="621365" timestamp="2008-12-26T21:51:29Z" version="1" visible="true" user="sw1210" uid="85611">
    <nd ref="321871158"/>
    <nd ref="321871159"/>
    <nd ref="321871161"/>
    <tag k="created_by" v="Potlatch 0.10f"/>
    <tag k="highway" v="service"/>
    <tag k="name" v="Am Isarkanal"/>
  </way>
  <way id="29270328" changeset="840930" timestamp="2009-01-26T21:18:29Z" version="2" visible="true" user="Czmartin" uid="43021">
    <nd ref="21512030"/>
    <nd ref="321871158"/>
    <nd ref="321871159"/>
    <nd ref="321871161"/>
    <tag k="created_by" v="Potlatch 0.10f"/>
    <tag k="highway" v="service"/>
    <tag k="name" v="Am Isarkanal"/>
  </way>
  <way id="29270328" changeset="2654507" timestamp="2009-09-27T17:07:47Z" version="3" visible="true" user="sw1210" uid="85611">
    <nd ref="21512030"/>
    <nd ref="321871158"/>
    <nd ref="321871159"/>
    <nd ref="321871161"/>
    <tag k="highway" v="service"/>
    <tag k="name" v="Am Isarkanal"/>
  </way>
</osm>
"""

XMLERROR = """<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="OpenStreetMap server" copyright="OpenStreetMap and contributors" attribution="http://www.openstreetmap.org/copyright" license="http://opendatacommons.org/licenses/odbl/1-0/">
  <way id="29270328" changeset="621365" timestamp="2008-12-26T21:51:29Z" version="thisisnotint" visible="true" user="sw1210" uid="85611">
    <nd ref="321871158"/>
    <nd ref="321871159"/>
    <nd ref="321871161"/>
    <tag k="created_by" v="Potlatch 0.10f"/>
    <tag k="highway" v="service"/>
    <tag k="name" v="Am Isarkanal"/>
  </way>
  <way id="29270328" changeset="840930" timestamp="2009-01-26T21:18:29Z" version="2" visible="true" user="Czmartin" uid="43021">
    <nd ref="21512030"/>
    <nd ref="321871158"/>
    <nd ref="321871159"/>
    <nd ref="321871161"/>
    <tag k="created_by" v="Potlatch 0.10f"/>
    <tag k="highway" v="service"/>
    <tag k="name" v="Am Isarkanal"/>
  </way>
  <way id="29270328" changeset="2654507" timestamp="2009-09-27T17:07:47Z" version="3" visible="true" user="sw1210" uid="85611">
    <nd ref="21512030"/>
    <nd ref="321871158"/>
    <nd ref="321871159"/>
    <nd ref="321871161"/>
    <tag k="highway" v="service"/>
    <tag k="name" v="Am Isarkanal"/>
  </way>
</osm>
"""


class oshParserTests(unittest.TestCase):

    def foo(self, foobar):
        return None

    def test_init(self):
        """
        """
        osp = oshParser()
        self.assertEqual(osp.nb_versions, 0)

    @patch('oshparser.oshparser.oshParser.readfile', Mock(return_value=XML))
    def test_getroot(self):
        osp = oshParser()
        root = osp.getroot('foobar')
        self.assertEqual(len(root.getchildren()), 3)
        self.assertEqual(root.tag, 'osm')

    @patch('oshparser.oshparser.oshParser.readfile', Mock(return_value=XML))
    def test_parse(self):
        osp = oshParser()
        res = osp.parse(osp.getroot('osm'))

        self.assertEqual(osp.nb_versions, 3)

    @patch('oshparser.oshparser.oshParser.readfile', Mock(return_value=XML))
    def test_parsefile(self):
        osp = oshParser()
        res = osp.parsefile("/dev/null")

        self.assertEqual(osp.nb_versions, 3)

    @patch('oshparser.oshparser.oshParser.readfile', Mock(return_value=XML))
    def test_pelement(self):
        osp = oshParser()
        root = osp.getroot("/dev/null")
        childs = root.getchildren()

        attr = osp.pelement(childs[0])
        self.assertEqual(attr['changeset'], "621365")

    @patch('oshparser.oshparser.oshParser.readfile', Mock(return_value=XMLERROR))
    def test_pelement_keyerror(self):
        osp = oshParser()
        root = osp.getroot("/dev/null")
        childs = root.getchildren()

        attr = osp.pelement(childs[0])
        self.assertEqual(attr['changeset'], "621365")
