#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import logging
import logging.handlers
import xml.etree.ElementTree as etree

__version__ = '0.2.0'


class oshParser(object):
    """Parse OpenStreetMap history files
    """
    logger = None
    node_callback = None
    users = []
    datas = []
    files_open = 0

    def __init__(self, **kwargs):
        self.init_logger(**kwargs)
        self.nb_versions = 0

    def init_logger(self, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger(__name__)

        if 'loglevel' in kwargs:
            self.logger.setLevel(kwargs['loglevel'])
        else:
            self.logger.setLevel(logging.CRITICAL)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def parsefile(self, filename):
        """Parse an xml file
        """
        self.datas = []
        # parse a file containing xml datas
        root = self.getroot(filename)
        self.parse(root)

    def getroot(self, filename):
        """Get the first element in xml datas
        """
        content = self.readfile(filename)

        try:
            tree = etree.fromstring(content)
            return tree
        except:
            msg = "Can't parse %s" % (filename)
            self.logger.critical(msg)
            sys.exit(1)

    def readfile(self, filename):
        """Read a file and return it's content

        - filename (string): the filename
        """
        try:
            self.files_open += 1
            # Parse a gzip file
            f = open(filename, 'r')
            content = f.read()
            f.close()
            return content
        except IOError, errno:
            self.logger.critical(errno)
            self.logger.critical("Exit here")
            sys.exit(1)
        except:
            msg = "Can't open %s : %s" % (filename, sys.exc_info()[0])
            self.logger.critical(msg)
            return ""

    def parse(self, root):
        """Get the xml root
        """
        for item in root.getchildren():
            self.nb_versions = self.nb_versions + 1
            self.pelement(item)

    def pelement(self, item):
        """Parse an element

        action :
        - modify
        - delete
        - create

        Returns JSON
        tags = []

        type = node/way/relation
        action = modify/delete/create
        """
        tags = []
        attr = {'type': item.tag}
        for key in item.keys():
            try:
                if key == 'version':
                    attr[key] = int(item.attrib[key])
                elif key == 'visible':
                    attr[key] = (item.attrib[key] == 'true')
                elif key == 'lon' or key == 'lat':
                    attr[key] = float(item.attrib[key])
                else:
                    attr[key] = item.attrib[key]
            except:
                self.logger.critical("Unexpected error: %s on %s" % (sys.exc_info()[0], key))

        # Add all the tags
        for child in item.getchildren():
            if child.tag == 'tag':
                tags.append({child.attrib['k']: child.attrib['v']})

        attr['tags'] = tags
        self.datas.append(attr)
        return attr

if __name__ == "__main__":
    osh = oshParser()
    osh.parsefile(sys.argv[1])
