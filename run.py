#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
from optparse import OptionParser
import osmreplicat
import ocheck

__version__ = "1.2.1"


def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-s SEQUENCE] [-v]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)

    parser.add_option("-i", "--input", dest="input",
                      type="string",
                      help="file name to parse",
                      default=None)
    parser.add_option("-d", "--dbname", dest="dbname",
                      help="database name",
                      default='oscstats')
    parser.add_option("-l", "--last-file", dest="lastfile",
                      help="name of lastfile (default lastdata.osc)",
                      type='string',
                      default='lastdata.osc')
    parser.add_option("-p", "--prefix", dest="prefix",
                      help="directory name prefix to store datas",
                      type='string',
                      default='./')
    parser.add_option("-r", "--replicat", dest="replicat",
                      help="replicat type",
                      type='string',
                      default='minute')
    parser.add_option("-s", "--sequence", dest="sequence",
                      type=int,
                      help="sequence number",
                      default=0)
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="be verbose",
                      default=False)

    return parser.parse_args()[0]


def main(options):
    """
    Main
    """
    fpath = osmreplicat.main(options)
    if fpath is not None:
        options.input = fpath
        ocheck.main(options)
        #  sys.stdout.write("%s\n" % (fpath))
        sys.exit(0)
    else:
        sys.exit(1)


if __name__ == '__main__':
    options = arg_parse()
    main(options)
