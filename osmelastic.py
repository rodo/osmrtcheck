# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014,2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import logging.handlers
import re
import requests
import json


class osmElastic(object):

    osmdl = None
    loglevel = logging.CRITICAL

    def __init__(self, conf, **kwargs):
        self.init_logger(**kwargs)
        self.config = conf

    def stop(self):
        self.logger.info("stop normally")

    def init_logger(self, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger("osmElastic")

        if 'loglevel' in kwargs:
            self.loglevel = kwargs['loglevel']

        self.logger.setLevel(self.loglevel)
        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def store_changeset(self, chg):
        """Store changesets in DB

        - chgt (dict)

        """
        url = "%s/%s" % (self.get_base_url("changeset"), chg['osmid'])

        datas = self.builddatas(chg)

        req = requests.put(url, data=json.dumps(datas))
        return req.status_code

    def get_base_url(self, etype):
        base = "%s:%s/%s/%s" % (self.config['elastic']['host'],
                                self.config['elastic']['port'],
                                self.config['elastic']['index'],
                                etype)
        return base

    def store_node(self, chg, node):
        """Store node in DB

        - chg (dict)
        """
        res = 1
        base = self.get_base_url("node")

        url = "%s/%sv%s" % (base, node['id'], node['version'])
        tags = []

        for tag in node['tags']:
            tags.append({'key': tag.keys()[0],
                         'value': tag.values()[0]})
            
        datas = {"changeset": self.minichg(chg),
                 "tags": tags,
                 "created_at": chg['created_at'],
                 "geom": node["geom"],
                 "action": node["action"],
                 "version": node["version"]}

        req = requests.put(url, data=json.dumps(datas))
        if req.status_code == 200:
            res = 0
        return res

    def builddatas(self, chg):

        comment = None
        task = None
        if 'comment' in chg['tags']:
            comment = chg['tags']['comment']
            task = self.hotosmtask(comment)

        datas = {"user": chg['other']['user'],
                 "created_at": chg['other']['created_at'],
                 "uid": chg['uid'],
                 "comment": comment,
                 "hotosm-task": task
                 }

        return datas


    def minichg(self, chg):

        comment = None
        task = None
        if 'comment' in chg['tags']:
            comment = chg['tags']['comment']
            task = self.hotosmtask(comment)

        datas = {"comment": comment,
                 "hotosm-task": task}

        return datas


    def hotosmtask(self, comment):

        result = None

        m = re.search('#hotosm-task-([0-9]+)', comment)
        if m:
            result = int(m.group(1))

        return result
