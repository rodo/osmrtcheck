#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap object downloader
"""
#
import os
import logging
import logging.handlers
from time import sleep
from requests import Session
from requests.exceptions import ConnectionError
from contextlib import closing


class osmDownloader(object):

    retry_after = 2
    loglevel = logging.CRITICAL
    api_host = "http://www.openstreetmap.org"
    url_node = "%s/api/0.6/node/{}/history" % (api_host)
    url_chgt = "%s/api/0.6/changeset/{}" % (api_host)

    def __init__(self, verbose=False, **kwargs):
        self.verbose = verbose
        self.init_logger(**kwargs)
        self.logger.info('url_node: {}'.format(self.url_node))
        self.logger.info('loglevel: {}'.format(self.loglevel))
        self.files_store = 0
        self.url_fetch = 0
        self.url_errors = 0
        self.init_session()
        
        if 'retry_after' in kwargs:
            self.retry_after = kwargs['retry_after']

    def init_session(self):
        """Initialize url session
        """
        self.session = Session()
        self.session.headers.update({'User-Agent': 'osmrtcheck'})

    def stop(self):
        self.logger.info("files store : %d" % (self.files_store))
        self.logger.info("urls fetch : %d" % (self.url_fetch))
        if self.url_errors > 0:
            self.logger.warning("url in errors : %d" % (self.url_errors))
        self.logger.info("stop normally")

    def init_logger(self, **kwargs):
        """Init logger
        """
        facility = logging.handlers.SysLogHandler.LOG_LOCAL5
        self.logger = logging.getLogger("osmDownloader")

        if 'loglevel' in kwargs:
            self.loglevel = kwargs['loglevel']

        self.logger.setLevel(self.loglevel)

        fmt = " ".join(['[%(process)d] %(name)s:',
                        '%(levelname)s %(message)s'])
        formatter = logging.Formatter(fmt)

        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=facility)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def fetch_node(self, osmid, fpath):
        """Fetch history node, store result in file
        """
        headers = {'User-Agent': 'osmrtcheck'}

        url = self.url_node.format(osmid)

        if not os.path.isfile(fpath):
            self.store_data(url, headers, fpath)

        return fpath

    def fetch_changeset(self, cid):
        """Fetch a changeset
        """
        headers = {'User-Agent': 'osmrtcheck'}
        url = self.url_chgt.format(cid)
        content = self.fetch(url, headers)
        return content

    def fetch(self, url, headers):
        """Retreive node from API and store on local filesystem

        Return None in failure
        """
        content = None
        loop = 0
        self.url_fetch += 1
        while loop < 3 and content is None:
            try:
                with closing(self.session.get(url, stream=False)) as req:
                    self.logger.debug('GET %s return %s' % (url,
                                                            req.status_code))

                    if req.status_code == 200:
                        content = req.content
                    else:
                        self.url_errors += 1
                        content = ''
            except ConnectionError, errno:
                self.logger.error('%s on %s' % (errno, url))
                loop += 1
                sleep(self.retry_after)
        return content

    def store_data(self, url, headers, fpath):
        """Retreive node from API and store on local filesystem
        """
        self.url_fetch += 1
        with closing(self.session.get(url, stream=True)) as req:

            self.logger.debug('GET {} return {}'.format(url, req.status_code))

            if req.status_code == 200:
                self.files_store += 1
                f = open(fpath, 'wb')
                for chunk in req.iter_content(chunk_size=1024):
                    #  filter out keep-alive new chunks
                    if chunk:
                        f.write(chunk)
                        f.flush()
                f.close()
            else:
                self.url_errors += 1

        if self.verbose:
            print fpath, req.status_code

        return req.status_code
