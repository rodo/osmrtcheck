#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import responses
import unittest
from mock import Mock, patch
from osmelastic import osmElastic


CONF = {
    'elastic': {
        'host': 'http://es.foo.bar',
        'port': '9020',
        'index': 'osm'
        },
    'srid': 4326,
    'loglevel': 'INFO',
    'frontend_url': 'http://192.168.17.10:8000',
    'frontend_token': '0de7ba1a03e036c28ef2cab19ae2555d82e175ec',
    'osmrtcheck': {
        'dbname': 'osmrtcheck',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    'osm2pgsql':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    'osmstat':  {
        'dbname': 'osmalsace',
        'dbuser': 'rodo',
        'dbpass': 'rodo',
        'dbhost': 'localhost',
        'dbport': 5433,
        },
    }


CHG = {'closed_at': '2014-09-28T20:12:00Z',
       'created_at': '2014-09-28T20:11:58Z',
       'osmid': 25733679,
       'uid': 42,
       'user': 'Charb',
       'tags': {'created_by': 'iD 1.5.4',
                'imagery_used': 'Bing',
                "comment": "lorem ipsum"},
       'other': {'closed_at': '2014-09-28T20:12:00Z',
                 'created_at': '2014-09-28T20:11:58Z',
                 'min_lat': 45.1,
                 'min_lon': 11.2,
                 'max_lat': 45.7,
                 'max_lon': 11.4,
                 'user': 'Charb'}}

@patch('changeset.Changeset.initclasses', Mock(return_value=True))
class changesetTests(unittest.TestCase):

    def test_init(self):
        """
        """
        els = osmElastic(CONF)
        self.assertEqual(els.config, CONF)

    def test_builddatas_wocomment(self):
        """
        """
        els = osmElastic(CONF)
        chg = {'closed_at': '2014-09-28T20:12:00Z',
               'created_at': '2014-09-28T20:11:58Z',
               'osmid': 25733679,
               'uid': 42,
               'user': 'Charb',
               'tags': {'created_by': 'iD 1.5.4',
                        'imagery_used': 'Bing'},
               'other': {'closed_at': '2014-09-28T20:12:00Z',
                         'created_at': '2014-09-28T20:11:58Z',
                         'min_lat': 45.1,
                         'min_lon': 11.2,
                         'max_lat': 45.7,
                         'max_lon': 11.4,
                         'user': 'Charb'}}

        res = els.builddatas(chg)

        attend = {"user": "Charb",
                  "created_at": "2014-09-28T20:11:58Z",
                  "uid": 42,
                  "comment": None,
                  "hotosm-task": None}
        
        self.assertEqual(res, attend)

    def test_builddatas_wicomment(self):
        """The changeset contains a comment
        """
        els = osmElastic(CONF)

        chg = {'closed_at': '2014-09-28T20:12:00Z',
               'created_at': '2014-09-28T20:11:58Z',
               'osmid': 25733679,
               'uid': 42,
               'user': 'Charb',
               'tags': {'created_by': 'iD 1.5.4',
                        'imagery_used': 'Bing',
                        "comment": "lorem ipsum"},
               'other': {'closed_at': '2014-09-28T20:12:00Z',
                         'created_at': '2014-09-28T20:11:58Z',
                         'min_lat': 45.1,
                         'min_lon': 11.2,
                         'max_lat': 45.7,
                         'max_lon': 11.4,
                         'user': 'Charb'}}

        res = els.builddatas(chg)
    
        attend = {"user": "Charb",
                  "created_at": '2014-09-28T20:11:58Z',
                  "uid": 42,
                  "comment": "lorem ipsum",
                  "hotosm-task": None}
        
        self.assertEqual(res, attend)

    def test_builddatas_hotcomment(self):
        """The changeset contains a comment hot dedicated
        """
        els = osmElastic(CONF)
        chg = {'closed_at': '2014-09-28T20:12:00Z',
               'created_at': '2014-09-28T20:11:58Z',
               'osmid': 25733679,
               'uid': 42,
               'user': 'Charb',
               'tags': {'created_by': 'iD 1.5.4',
                        'imagery_used': 'Bing',
                        "comment": "#hotosm-task-804, lorem ipsum"},
               'other': {'closed_at': '2014-09-28T20:12:00Z',
                         'created_at': '2014-09-28T20:11:58Z',
                         'min_lat': 45.1,
                         'min_lon': 11.2,
                         'max_lat': 45.7,
                         'max_lon': 11.4,
                         'user': 'Charb'}}

        res = els.builddatas(chg)

        attend = {"user": "Charb",
                  "created_at": '2014-09-28T20:11:58Z',
                  "uid": 42,
                  "comment": "#hotosm-task-804, lorem ipsum",
                  "hotosm-task": 804}
        
        self.assertEqual(res, attend)

    def test_hotcomment(self):
        """
        """
        els = osmElastic(CONF)
        res = els.hotosmtask("#hotosm-task-804")

        attend = 804
        
        self.assertEqual(res, attend)

    def test_hotcomment_empty(self):
        """
        """
        els = osmElastic(CONF)
        res = els.hotosmtask("#hotosmtask-804")
        attend = None
        
        self.assertEqual(res, attend)

    @responses.activate
    def test_store_node(self):
        """
        """
        responses.add(**{
            'method': responses.PUT,
            'url': 'http://es.foo.bar:9020/osm/node/2v12',
            'body': '{"error": "reason"}',
            'status': 200,
            'content_type': 'application/json',
            'adding_headers': {'X-Foo': 'Bar'}
        })

        node = {'id': 2,
                'version': 12,
                'tags': [{'building': 'yes'}],
                'geom': 'POINT(3 3)',
                'action': 'create'
                }
        els = osmElastic(CONF)
        res = els.store_node(CHG, node)
        
        self.assertEqual(res, 0)
