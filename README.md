OpenStreetMap Realtime Checker
==============================

Files
-----

* osmreplicat.py, fetch the last diff on "planet.openstreetmap.org"
  and store the file on local filesystem

* checker.py, lookup for errors on osm file based on plugins (legacy)

* ocheck.py : new generation of osmrtcheck

* run.py enclose osmreplicat and ocheck.py

Debian
------

* apt-get install python-requests python-imposm-parser python-pysqlite2

Dependencies
------------

* osmconvert

  http://wiki.openstreetmap.org/wiki/Osmconvert

Usage
=====

ocheck.py
---------

ocheck.py is configured with a `config.py` present in same directory
as script, the file contains db configuration, srid, the API url and
token to access it.

sample `config.py`

<!-- language: python -->

conf = {
    'dbname': 'osmrtcheck',
    'dbuser': 'rodo',
    'dbpass': 'rodo',
    'dbhost': 'localhost',
    'dbport': 5432,
    'srid': 4326,
    'frontend_url': 'http://192.168.17.10:8000',
    'frontend_token': '0deba71a0303e628cef2abc19a25e55d82e175ec'
    }


