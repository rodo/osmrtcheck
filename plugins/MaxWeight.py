#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""
from osdichPlugin import osdichPlugin
import re


class MaxWeight(osdichPlugin):

    @classmethod
    def valid(self, value):
        msg = None
        if self.contains_exclusion(value):
            result = True
        else:
            (result, msg) = self.floatvalue(value)
        return (result, msg)

    @classmethod
    def floatvalue(self, value):
        msg = None
        if self.is_float(value):
            # The value is a positive value and may be float or integer
            result = float(value) > 0
        else:
            unit = value[-2:]
            data = value[:-2]  # remove last 2 chars
            if unit == ' t' and self.is_float(data):
                result = True
            else:
                msg = "maxweight wrong format {}".format(value)
                result = False
        return (result, msg)
