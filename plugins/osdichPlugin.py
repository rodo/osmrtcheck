#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Diff checker
"""

class osdichPlugin(object):

    @classmethod
    def is_float(self, data):
        try:
            float(data)
            return True
        except:
            return False

    @classmethod
    def contains_exclusion(self, data):
        """Some special values are used sometimes
        """
        exclusions = ['fixme', 'none']
        if data.lower() in exclusions:
            return True
        else:
            return False



