#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 OpenStreetMap Realtime Checker Plugin for maxheight
"""
from osdichPlugin import osdichPlugin
import re


class MaxHeight(osdichPlugin):

    @classmethod
    def valid(self, value):
        msg = None
        if self.contains_exclusion(value):
            result = True
        else:
            (result, msg) = self.floatvalue(value)
        return (result, msg)

    @classmethod
    def floatvalue(self, value):
        msg = None
        if self.is_float(value):
            # The value is a positive value and may be float or integer
            result = float(value) > 0
        else:
            (result, msg) = self.withunits(value)

        return (result, msg)

    @classmethod
    def withunits(self, value):
        """Look for a units
        """
        msg = None
        if value.endswith("m"):
            try:
                (data, unit) = value.split(" ")
                if unit == 'm' and self.is_float(data):
                    result = True
                else:
                    msg = "maxheight wrong format {}".format(value)
                    result = False
            except:
                msg = "maxheight wrong format {}".format(value)
                result = False
        else:
            prog = re.compile("[0-9]+'[0-9]+\"")
            if prog.match(value) is not None:
                result = True
            else:
                result = False
                msg = "maxheight wrong format {}".format(value)

        return (result, msg)
